# Spatial Analysis of Histology in 3D

Supplementary material for article Ruusuvuori et al. "Spatial Analysis of Histology in 3D: Quantification and Visualization of Organ and Tumor Level Tissue Environment"

https://www.dropbox.com/sh/3cr4icsvxsqokp7/AACGuYgIIETNz6fLhR2t_3ywa?dl=0


## Contents

This repository contains tools for:
- Registration and 3D reconstruction of whole slide images of histological sections. Grid search of registration parameters.
- Image conversion and resizing
- Viusalization histology & ROI areas using various methods such as UMAP, TSNE, PCA, Clustergams
- 2D histological feature extraction (e.g. nuclei and texture features) - Matlab
- 3D volume visualization - Matlab
- 3D shape feature extraction from ROI annotations
- 2D histoloial feature extraction using autoencoders
- Registration performance evaluation metric
- Landmark image reading and writing functions to transform a set of landmarks into same coordinate system with a registered image
- Tile tissue in WSI images into smaller images and reconstruct it back to full WSI


## 3D histology reconstruction

Automatic image registration pipeline for large histological section images

This registration pipeline is built around ImageJ/Fiji software distribution and more specifically the Elastic stack alignment plugin. 
The scripts for Fiji (and ESA) are written in python. 

This pipeline is intended to be used on a computational cluster that uses Slurm job scheduling system.

Although each module is executable by itself, it is recommended to just use the higher level command line tool (histo) that wraps most of the tools together.

Now the command `histo` should be usable in any directory. Test is by simply typing `histo`.


## Installation
First ssh into any computation environment using SLURM job scheduling system.

Then go into the directory you wish to store the program and execute the following command on your terminal/command line:
For example: `cd ~/Projects` and then get the pipeline code with
`git clone https://github.com/BioimageInformaticsTampere/3D-histology.git`

Update submodules (repositories within this repository) by running the following command inside biit_image_tools and biit-wsi-featureextraction directories:
`git submodule update --recursive --init`

Next create a symbolic link to your ~/bin/ directory to be able to run this script everywhere in the file system.
(On Narvi cluster, ~/bin is in PATH by default). 
`ln -s ~/<path to histo command> ~/bin/histo`

For example: 
`ln -s ~/Projects/3D-Histology/histo ~/bin/histo`

## Basic usage
The histo tool is designed to be run on the Slurm system's login node. Under the hood, computational tasks are sent to Slurm job scheduling system to be run on the cluster and these tasks are only monitored on the login node.

Run histo without any arguments to get help
`histo`

The command has the following subcommands:
- *align* - perform Elastic Stack Alignment registration on input images
- *reapply* - apply same transformations to mask images given the corresponding registered images and project file (tempproject.xml)
- *gsearch* - Find optimal parameters by grid searching parameter space
Some not properly tested functionality
- *register* - Not implemented yet. Use the full pipeline to convert files to correct file type, register images, and optionally reapply the same registration transforms to mask images
- *convert* - convert images e.g. from jp2 to tiff (In progress..)

You can find help for each subcommand by calling them without arguments
`histo align`



## Internals

The pipeline consists of two parts namely registration and mask processing that are executed under different environments. Jython is used for the registration branch, which is provided in the Fiji distribution. 
The mask processing branch uses CPython. Each module is a separate program and can be run individually through its own commandline interface. 
histo.py uses these interfaces to send jobs to slurm.


## Directory structure


### align
Image registration related scrips that perform Elastic Stack Alignment.

- align.py
  - Fiji/Jython script to perform ESA registration for a stack of images
  
- align_pair.py
  - Fiji/Jython script to perform ESA registration for an image pair

### biit_image_tools
Some common image processing operations

### biit_slurm
Slurm interface for python

### biit-wsi-featureextraction
2D histological feature extraction written in matlab

### biit-wsi-preprocessing
image preprocessing written in matlab

### biit_wsi_io
A python module to read and write common image formats, slide scanner formats, and JPEG2000 images.

### cli
Code for the histo.py command line tool

### conversion 
Image format conversion & resizing related scripts using many different tools.

### features
Contains all feature extraction *and visualization* code in the project. 
Includes e.g. autoencoder feature computation (nod used in manuscript), experimental 3D volume visualizations (not used in manuscript), bilinear matlab feature interpolation between features from adjacent sections and practically all visualization code for the project.

- *tsne* - This directory contains all visualizations (UMAP, T-SNE, PCA, clustergrams, etc.) written in python and bash scripts to execute them in slurm environment


### matlab 
Potentially useful scrips from an earlier study written in matlab. Contains e.g. visualization scripts for the registered image stacks.

### postprocess_masks
Code for mask postprecessing after reapplying transforms.

### preprocess_masks
Code for mask preprecessing before reapplying transforms.

### reapply transform 
Transform a set of images using existing ESA project file that contains elastic transformations for a stack of images. 
With these, new set of images can be brought into the same coordinate system with already registered image stack. 
This useful for example when aligning existing annotations with the registered stack.

### stack_evaluation
Code for automatic stack evaluation using Laplacian weighted squared error.

### tests
Not proper tests but rather some utility scripts to execute parts of the pipeline one a time in interactive mode to make debugging possible.

### tiling
WSI image tiling code

### work
All essential bash scripts to perform all the steps in the project written with reproducibility in mind. 
The scrips and directories are numbered to preserve chronological order.

The final results studied in the manuscript were computed with the scripts within this directory:
`work/9_full_res_register_and_reapply_best_GRID_SEARCH_stacks/all_mice`


## Limitations

There is a limitation for the maximum *output* resolution of the registration tool 2^31-1 due to integer overflow in the Fiji/Java implementation of Elastic Stack Alignment.
