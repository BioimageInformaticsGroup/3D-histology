import os
import sys
import pickle
import logging

import glymur
import numpy as np
# import matplotlib
# matplotlib.use("Agg")  # set backend for headless server
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import seaborn as sns

sys.path.insert(0, "../biit_wsi_io/")
import wsiio

logging.basicConfig(level=logging.DEBUG)

# OPTIONS
# if IMAGE_PATH_PREFIX is defined use some other path than the current working directory to display images
# IMAGE_PATH_PREFIX = None
IMAGE_PATH_PREFIX = "/home/masi/Projects/RSYNC_THIS/bmt-data/bii/projects/3D-histology/output/PTENX036-14/full_res_jp2"

MARKER_SIZE = 40
# Reduction level used to display image in the right side figure
TILE_DISPLAY_RL = 0
# subsampling factor for the number of samples
SUBSAMPLING_FACTOR = 6 # disable with 1

def load_figure_data(load_path: str) -> dict:
    with open(load_path, 'rb') as f:
        data = pickle.load(f)
    return data


def main():

    data = load_figure_data(sys.argv[1])

    # ok = np.array(data['wincoords']).reshape((len(data['wincoords']), 2, 2))
    # logging.debug("max ymin: {}".format(ok[:, 0, 0].max()))
    # logging.debug("max xmin: {}".format(ok[:, 1, 0].max()))
    # logging.debug("max ymax: {}".format(ok[:, 0, 1].max()))
    # logging.debug("max xmax: {}".format(ok[:, 1, 1].max()))
    # logging.debug("Image dimensions: {}".format(lskdjaksj))
    # sys.exit()

    # subsample
    if SUBSAMPLING_FACTOR > 1:
        data['X'] = data['X'][::SUBSAMPLING_FACTOR]
        data['plot_labels'] = data['plot_labels'][::SUBSAMPLING_FACTOR]
        data['display_image_paths'] = data['display_image_paths'][::SUBSAMPLING_FACTOR]
        data['wincoords'] = data['wincoords'][::SUBSAMPLING_FACTOR]

    sns.set_style("darkgrid")
    if data["X"].shape[1] == 2:
        logging.info("Data dimensionality: 2 dimensions")

        fig, ax = plt.subplots()
        sc = plt.scatter(data['X'][:, 0], data['X'][:, 1], s=MARKER_SIZE)

    elif data["X"].shape[1] == 3:
        logging.info("Data dimensionality: 3 dimensions")

        fig = plt.figure()
        ax = fig.add_subplot(121, projection='3d')
        sc = ax.scatter(data['X'][:, 0], 
                        data['X'][:, 1], 
                        data['X'][:, 2], s=MARKER_SIZE)
        ax2 = plt.subplot(122)
        ax2.imshow(np.zeros((1,1)))
        plt.axis('off')
    else: 
        logging.warn("Data dimensionality not 2 or 3")

    annot = ax.annotate("", 
                        xy=(0,0), 
                        xytext=(20,20),
                        textcoords="offset points",
                        bbox=dict(boxstyle="round", fc="w"),
                        arrowprops=dict(arrowstyle="->"))
    annot.set_visible(False)

    def update_annot(ind):
        pos = sc.get_offsets()[ind["ind"][0]]
        annot.xy = pos
        text = "{}".format(data["plot_labels"][ind["ind"]])
        annot.set_text(text)
        # annot.get_bbox_patch().set_facecolor(cmap(norm(c[ind["ind"][0]])))
        annot.get_bbox_patch().set_alpha(0.4)


    def hover(event):
        empty_image = np.zeros((1,1))
        vis = annot.get_visible()
        if event.inaxes == ax:
            cont, ind = sc.contains(event)
            if cont:
                update_annot(ind)
                annot.set_visible(True)
                fig.canvas.draw_idle()
                # read tile directly from correct jp2 image by using jp2 random access support
                first_ind = ind['ind'][0] 
                impath = data['display_image_paths'][first_ind]
                if IMAGE_PATH_PREFIX is not None:
                    impath = os.path.join(IMAGE_PATH_PREFIX, os.path.basename(impath))

                wincoords = data['wincoords'][first_ind]


                # wincoords are need in format (first_row, first_col, last_row, last_col)
                wincoords = (wincoords[0], wincoords[2], wincoords[1], wincoords[3])

                # display larger window around sample
                # s = 200
                # wincoords = (wincoords[0] - s, wincoords[1] - s, wincoords[2] + s, wincoords[3] + s)
                # # read jp2 file size from disk
                # image_size = glymur.Jp2k(impath).shape[:2]
                # wincoords = np.array(wincoords)
                # wincoords[0] = np.max((wincoords[0], 0))
                # wincoords[1] = np.max((wincoords[1], 0))
                # wincoords[2] = np.min((wincoords[2], image_size[0]))
                # wincoords[3] = np.min((wincoords[3], image_size[1]))

                # print("impath: {}".format(impath))
                # print("inds: {}".format(ind))
                # print("first_ind: {}/{}".format(first_ind, len(data['plot_labels'])))
                # print("wincoords: {}".format(wincoords))
                # print("winsize: {}x{}".format(wincoords[2] - wincoords[0], wincoords[3] - wincoords[1]))

                tile, full_res_dims = wsiio.biitwsi_imread(impath, TILE_DISPLAY_RL, wincoords)

                # # tile = tile[wincoords[0]:wincoords[2], wincoords[1]:wincoords[3], :]
                # # print("Exists: {}".format(os.path.exists(impath)))
                # print("Tile mean: {}".format(tile.mean()))
                # print("Tile shape: {}".format(tile.shape))
                # # print("Tile dtype: {}".format(tile.dtype))
                # # print("Tile variance: {}".format(np.var(tile[:])))
                # sys.stdout.flush()

                ax2.imshow(tile)

            else:
                if vis:
                    annot.set_visible(False)
                    fig.canvas.draw_idle()
                    ax2.imshow(empty_image)

    fig.canvas.mpl_connect("motion_notify_event", hover)

    plt.show()

if __name__ == '__main__':
    main()


