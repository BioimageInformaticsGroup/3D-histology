#!/usr/bin/env python

import sys
import re
import os
import copy
import time
import logging
from pprint import pprint
from argparse import ArgumentParser
# form argparser import ArgumentParser

import pandas as pd
import pickle
import natsort
import numpy as np
import scipy.io as sio
from scipy.interpolate import interp1d
from sklearn.manifold import TSNE
# import matplotlib
# matplotlib.use("Agg")  # set backend for headless server
import matplotlib.pyplot as plt
# import seaborn as sns
from matplotlib.offsetbox import OffsetImage, AnnotationBbox


TSNE_DIMS = 3
PLOT_DATA_DIR = "plot_data"

def save_data(X: np.ndarray, 
              plot_labels: list, 
              display_image_paths: str, 
              wincoords: list, 
              path: str) -> None:
    """ Save plot data to disk for later plotting

    Args:
        X(np.ndarray): Data to scatterplot, rows = samples, columns = dimensions
        plot_labels(list(str)): 1d list of strings. data to display on mouseover 
        display_image_paths(list(str)): paths to images that are 
                                        interactively shown on mouseover
        wincoords(list(np.ndarray)): 1d list of 2d numpy arrays, 
                                     block corner coordinates, 
                                     [[ymin, ymax], [xmin, xmax]]

    """

    savedir = os.path.dirname(path)
    if not os.path.exists(savedir):
        os.makedirs(savedir)

    data = {"X":X, 
            "plot_labels":plot_labels, 
            "wincoords":wincoords, 
            "display_image_paths":display_image_paths}

    with open(path, 'wb') as f:
        pickle.dump(data, f)


def print_mem_usage():
    import sys
    import psutil
    process = psutil.Process(os.getpid())
    # output in megabytes
    print("Memory usage: {:.2f} MiB".format(process.memory_info().rss / (1024 * 1024)))
    sys.stdout.flush()


def load_data(fns, num_feats, dict_key, path):
    X = np.empty((0, num_feats))
    X_filenames = [] 
    X_winxy = [] 
    X_blockcorners = []
    tic()

    # get size from the first file and assume same size over all .mat files
    slice_path = os.path.join(path, fns[0])
    slice1_data = sio.loadmat(slice_path) 
    y_max, x_max = slice1_data[dict_key].shape
    blockcorners = slice1_data['blockcorners'].ravel()
    rng = list(range(np.prod((y_max, x_max))))
    # compute window x,y locations for every tile
    window_tile_indices = np.array(np.unravel_index(rng, (y_max, x_max))).transpose()

    start_ind = 0
    end_ind = 0
    for i in range(len(fns)-1):
        logging.info("Processing slice: {}/{}".format(i+1, len(fns)-1))
        print_mem_usage()

        slice_path = os.path.join(path, fns[i])
        slice1_data = sio.loadmat(slice_path) 

        y_m, x_m = slice1_data[dict_key].shape
        assert(y_m == y_max)
        assert(x_m == x_max)

        # accumulate all features into one array from tiles that has tissue (i.e. features present)
        dat = np.ravel(slice1_data[dict_key])
        has_features = [d.size != 0 for d in dat]
        dat = dat[has_features]
        # get also tile indices from the corresponding locations
        tile_inds = window_tile_indices[has_features]
        # make ndarray of shape (x, 250) or (x, 602)
        dat = np.concatenate(dat)
        X = np.concatenate((X, dat))
        X_filenames.extend([fns[i]]*len(dat))
        X_winxy.extend(tile_inds)
        X_blockcorners.extend(blockcorners[has_features])
    
    # get only basename
    X_filenames = [os.path.basename(fn) for fn in X_filenames]

    # get rid of INFS and NANS 
    X[np.isnan(X)] = 0
    X[np.isinf(X)] = 0
    toc()

    return X, np.array(X_filenames).reshape((len(X_filenames), 1)), np.array(X_winxy), np.array(X_blockcorners).reshape((len(X_blockcorners), 4))


def get_cli_args():
    parser = ArgumentParser(description="Plot TSNE from histological feature data")
    parser.add_argument('path', 
            type=str, help='path to features (.mat files)')
    parser.add_argument('feature_data_str', 
            type=str, help='features_block or features_nuclei')
    parser.add_argument('num_features', 
            type=int, help='number of features in each window \
            (features_block = 250, features_nuclei = 602)')
    args = vars(parser.parse_args())

    # remove trailing slash if exists
    if args['path'][-1] == "/":
        args['path'] = args['path'][:-1]


    logging.info("Printing cli arguments")
    logging.info("    {}:{}".format("path", args["path"]))
    logging.info("    {}:{}".format("feature_data_str", args["feature_data_str"]))
    logging.info("    {}:{}".format("num_features", args["num_features"]))

    return args

if __name__=='__main__':
    logging.basicConfig(level=logging.INFO)

    args = get_cli_args()
    fns = os.listdir(args['path'])
    fns = natsort.natsorted(fns)
    pprint(fns)
    fns = [os.path.join(args['path'], fn) for fn in fns]
    
    X, X_filenames, X_winxy, wincoords = load_data(fns, args['num_features'], args['feature_data_str'], args['path'])

    # labels to show when mouseovering a sample
    labels = np.concatenate((X_filenames, X_winxy), axis=1)

    tsne = TSNE(n_components=TSNE_DIMS, verbose=1, perplexity=40, n_iter=300)
    # tsne = TSNE(n_components=2, verbose=1, perplexity=40)
    X_t = tsne.fit_transform(X)

    logging.debug("Number of tsne iterations run: {}".format(tsne.n_iter))
    logging.debug("X_t shape: {}".format(X_t.shape))
    logging.debug("labels shape: {}".format(labels.shape))

    print_mem_usage()

    # Save plot data
    mouse = args['path'].split(os.sep)[-2]
    feat_dirname = args['path'].split(os.sep)[-1]
    save_path = os.path.join(PLOT_DATA_DIR, "{}_{}_{}_{}".format(mouse, 
                                                        feat_dirname, 
                                                        args['feature_data_str'],
                                                        "TSNE{}.pkl".format(TSNE_DIMS)))

        
    # paths to jpg images from which the tiles are read on the fly 
    # to show the right hand side interactive til window/view
    tile_paths = [str(fnam[0][9:-4]) + ".jp2" for fnam in X_filenames]
    tile_paths = [os.path.join("plot_data", "images", fnam) for fnam in tile_paths]

    save_data(X_t, labels, tile_paths, wincoords, save_path)

    import pdb; pdb.set_trace()
