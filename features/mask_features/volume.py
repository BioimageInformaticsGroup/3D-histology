import sys
import logging

import numpy as np
from typing import List

sys.path.insert(0, "../")
from utils.utils import get_mem_usage

logging.basicConfig(level=logging.DEBUG)

class Volume:

    # assuming symmetric voxel shape along each dimension (in micro meters)
    _voxel_size_symmetric_um = None

    # in micro meters
    _microns_between_slices = None

    # pixels between slices
    _pixels_between_slices = None

    # lesion's label (1, 2, 3 ...)
    _label = None

    # scale of internl rperesentation of Volume, bounding box coords and images have this scale
    # 1 == no scaling, 0.5 == 50% subsampling
    _scaling_coeff = None

    # bounding cube coordinates (zmin, zmax, ymin, ymax, xmin, xmax)
    _bounding_cube = None

    # slice images one after another in an ndarray
    # spacing between sections along z dimensions does not match reality in this representation
    _stack = None

    # slice images in an ndarray that represents correct distances between slices.
    # There is thickness_px amount of zeros in between each slice
    _volume = None

    # true z indices of stack images in pixels with symmetric voxlel size in each dimension
    _true_z_indices = None

    def __init__(self,
                 image_stack: np.ndarray,
                 scale_percentge: float,
                 bounding_cube_coordinates: tuple,
                 stack_filenames: List[str],
                 voxel_size_symmetric_um: float = 0.46,
                 microns_between_slices: float = 50.0):
        """ Create Volume object

            Args:
                scale_percentage: scale percentage for images
                bounding_cube_coordinates: in WSI coordinates

        """
        # get label before removing labels
        self._label = image_stack.max()
        # save filenames
        self._stack_filenames = stack_filenames
        # get rid of labels
        self._stack = (image_stack > 0).astype(np.uint8)

        self._voxel_size_symmetric_um = voxel_size_symmetric_um
        self._microns_between_slices = microns_between_slices
        # scale of images in _stack w.r.t. WSI scale
        self._scaling_coeff = scale_percentge / 100.0
        # coorinates of bounding cube (zmin, zmax, ymin, ymax, xmin, xmax)
        # to get WSI scale, divide with _scaling_coeff
        self._bounding_cube = bounding_cube_coordinates
        # yapf: disable
        # subsampled scale, divide with scale_coeff to get WSI scale
        self._pixels_between_slices = (self._scaling_coeff
                                       * self._microns_between_slices
                                       / self._voxel_size_symmetric_um)
        # yapf: enable
        # in subsampled scale, divide by scaling_coeff to get WSI scale
        self._true_z_indices = self._compute_true_z_indices()
        logging.info("Created volume object: {}".format(self))

    def __repr__(self):
        return "Volume object\n"\
               "  Microns between slices: {}\n"\
               "  Pixels between slices: {}\n"\
               "  Scaling coeff: {}\n"\
               "  label: {} <- this is incorrect, TODO\n"\
               "  scale bounding cube (zmin, zmax, ymin, ymax, xmin, xmax): {}\n"\
               "  stack shape: {}\n"\
               "  true_z_coordinates_px: {}\n"\
               "  volume shape: {}\n"\
               .format(self._microns_between_slices,
                       self._pixels_between_slices,
                       self._scaling_coeff,
                       self._label,
                       self._bounding_cube,
                       self._stack.shape,
                       self._true_z_indices,
                       None if self._volume is None else self._volume.shape)

    def _make_volume(self, image_stack: np.ndarray, slice_thickness_px: float):
        # yapf: disable
        """create ndarray with slice_thickness_px amount of empty layers in between each layer"""
        num_slices = image_stack.shape[0]
        z_out = int(np.ceil(slice_thickness_px)) * num_slices
        new_stack = np.zeros((z_out, image_stack.shape[1], image_stack.shape[2]))
        for i in range(num_slices):
            # round the position of the images in the array
            pos = int(np.round(slice_thickness_px * i))
            new_stack[pos, :, :] = image_stack[i, :, :]
        return new_stack
        # yapf: enable

    def _compute_true_z_indices(self):
        num_slices = self._stack.shape[0]
        z_out = self._bounding_cube[0] + np.arange(
            num_slices) * self._pixels_between_slices
        return z_out

    # basic getters

    def get_stack(self):
        """get stack of 2d images (spacing along z dimension not correct)"""
        return self._stack

    def get_filenames(self):
        """get filenames corresponding to layers in stack"""
        return self._stack_filenames

    def get_bounding_cube_coords(self):
        """(zmin, zmax, ymin, ymax, xmin, xmax)"""
        return self._bounding_cube

    def get_scaling_coeff(self):
        return self._scaling_coeff

    def get_label(self):
        return self._label

    def get_pixels_between_slices(self):
        return self._pixels_between_slices

    def get_full_res_microns_between_slices(self):
        return self._microns_between_slices

    def get_full_res_voxel_size_symmetric_um(self):
        return self._voxel_size_symmetric_um

    # some computation getters

    def get_nonzero_volume_layer_idx(self):
        """ layer idx is the same a index along z dimension"""
        return np.where(self._volume.max(axis=2).max(axis=1))[0]

    def get_volume(self):
        """get volume (with correct spacing between slices along z dimension)"""
        if self._volume is None:
            logging.debug("Memory usage before make dense: {}".format(
                get_mem_usage()))
            self._volume = self._make_volume(self._stack,
                                             self._pixels_between_slices)
            logging.debug("Memory usage after make dense: {}".format(
                get_mem_usage()))
        return self._volume

    def layer_iterator(self):
        """ Get layers and corresponding coordinates in pixels """
        z_idx = self._true_z_indices
        # ymin & xmin
        y = self._bounding_cube[2]
        x = self._bounding_cube[4]
        for z, im in zip(z_idx, self._stack):
            yield z, y, x, im
