import os
import re
import sys
import time
import socket
import logging
from typing import Dict, List
from argparse import ArgumentParser

import matplotlib
if socket.gethostname() != "x1g6":
    matplotlib.use('Agg')
import numpy as np
from numpy.linalg import norm
from PIL import Image
# from scipy.misc import imread
from skimage.io import imread
from scipy.spatial import ConvexHull, distance, Voronoi
from scipy.ndimage import center_of_mass
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from matplotlib.patches import Polygon
from sklearn.decomposition import PCA
import skimage.morphology
from skimage.measure import marching_cubes_lewiner

sys.path.insert(0, "../../")
sys.path.insert(0, "../")

from utils.utils import (end_timing, extract_sequence_numbers_from_filenames,
                         filter_filelist_with_regex, print_mem_usage,
                         start_timing, extract_mouse_identifier)


# without ptiles
from featstats import feature_statistics_less as feature_statistics
# # with ptiles
# from featstats import feature_statistics

from extract_volumes import Volume
from compress import load_compressed_object, load_object
# from hks import getHKS

Image.MAX_IMAGE_PIXELS = 5000000000
logging.basicConfig(level=logging.DEBUG)

# # Not registered
# anatomical_centers = [
#     { 'filename': "Lat-186_PTENX036-20-37_18114_46028_7452.tiff.jp2", 'locx': 7500.01171875, 'locy': 7071.949218750002, 'mouse_id':'PTENX036-20', },
#     { 'filename': "Lat-186_PTENX036-15-43_19599_20556_9932.tiff.jp2", 'locx': 8178.8662109375, 'locy': 10918.8955078125, 'mouse_id':'PTENX036-15', },
#     { 'filename': "Lat-186_Ptenx036-14-37_3855_4284_4156.tiff.jp2", 'locx': 7430.30859375, 'locy': 5981.800781249998, 'mouse_id':'PTENX036-14', },
#     { 'filename': "Lat-186_PtenX036-011-28_17678_21900_24556.tiff.jp2", 'locx': 5602.433593749999, 'locy': 7132.015625, 'mouse_id':'PTENX036-011', },
#     { 'filename': "LAT-186_PTENX036-033-37_6716_3964.tiff.jp2", 'locx': 6883.2841796875, 'locy': 10773.7373046875, 'mouse_id':'PTENX036-033', },
#     { 'filename': "LAT-186_PTENX036-032-37_28079_4076_5340.tiff.jp2", 'locx': 10925.0361328125, 'locy': 5144.6630859375, 'mouse_id':'PTENX036-032', },
# ]

# 50% subsample,d registered (reapplied) annotations coordinates
anatomical_centers = [
        { 'filename': "Transformed_Preprocessed_landmarks_Lat-186_PtenX036-011-28_17678_21900_24556.png", 'locx': 5753.0, 'locy':4892.5, 'mouse_id':"PTENX036-011"},
        { 'filename': "Transformed_Preprocessed_landmarks_Lat-186_Ptenx036-14-37_3855_4284_4156.png", 'locx': 5690.0, 'locy':5309.5, 'mouse_id':"PTENX036-14"},
        { 'filename': "Transformed_Preprocessed_landmarks_Lat-186_PTENX036-15-43_19599_20556_9932.png", 'locx': 7550.5, 'locy':7082.0, 'mouse_id':"PTENX036-15"},
        { 'filename': "Transformed_Preprocessed_landmarks_Lat-186_PTENX036-20-37_18114_46028_7452.png", 'locx': 7795.0, 'locy':5438.5, 'mouse_id':"PTENX036-20"},
        { 'filename': "Transformed_Preprocessed_landmarks_LAT-186_PTENX036-032-37_28079_4076_5340.png", 'locx': 5578.0, 'locy':4671.5, 'mouse_id':"PTENX036-032"},
        { 'filename': "Transformed_Preprocessed_landmarks_LAT-186_PTENX036-033-37_6716_3964.png", 'locx': 6256.5, 'locy':5590.5, 'mouse_id':"PTENX036-033"},
]


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("volumes_dir",
                        help="Path to volume objects directory",
                        type=str)
    parser.add_argument("--output_csv",
                        help="Path to extracted features table",
                        default="features.csv",
                        type=str)
    args = vars(parser.parse_args())
    return args


def check_arguments(args):
    if not os.path.exists(args["volumes_dir"]):
        logging.info("Path to volumes_dir does not exist", file=sys.stderr)
        sys.exit(1)

    # make sure output dir exists
    os.makedirs(os.path.dirname(args["output_csv"]), exist_ok=True)


def get_vectors_from_axis_to_voxels(axis, voxel_coordinates):
    from_axis_to_voxel_vectors = []
    b = axis # pca.components_[0, :]  # 1st pca component
    for a in voxel_coordinates:
        a1 = np.dot(a, b / norm(b)) * b
        a2 = a - a1  # a2 points from pca axis to voxel
        assert (np.isclose(np.dot(a1, a2), 0)), "a1 and a2 should be othogonal"
        from_axis_to_voxel_vectors.append(a2)
    return np.array(from_axis_to_voxel_vectors)


def distance_between_tumor_and_point(lesion: Volume, point: np.ndarray):
    """Computations are done in prostate coordinate system"""
    coords3 = get_volume_coords_in_prostate_coord_system(lesion)
    dists = np.array([norm(point - c) for c in coords3])
    return min(dists)


def get_volume_coords_in_prostate_coord_system(v: Volume):
    coords_prostate_system = []
    # z, y, x are LAYER top left coordinates in prostate system
    for z, y, x, im in v.layer_iterator():
        coords = np.array(np.where(im > 0)).T
        coords = coords + [y, x]
        coords3 = np.concatenate((np.ones((coords.shape[0], 1))*z, coords), axis=1)
        coords_prostate_system.extend(coords3.tolist())
    return coords_prostate_system


def shortest_dist_voronoi(smaller_set: np.ndarray, larger_set: np.ndarray):
    import pdb; pdb.set_trace()
    diag = Voronoi(np.array(lcoords), furthest_site=False, incremental=False, qhull_options=None)
    import pdb; pdb.set_trace()


def distance_between_tumor_and_prostate_boundaries(lesion: Volume, prostate: Volume, lesion_features: dict):
    """Finds minimum distance between tumor and prostate boundaries with exhaustive search"""

    # find all tumor coordinates
    st = time.time()
    lcoords = get_volume_coords_in_prostate_coord_system(lesion)
    print(f"lcoords took {time.time() - st}s")

    # find all prostate coordinates and distances to tumor center of mass
    st = time.time()
    pcoords = get_volume_coords_in_prostate_coord_system(prostate)
    print(f"pcoords took {time.time() - st}s")

    # # find closest pair ~600-900s
    # st = time.time()
    # dists = np.array(distance.cdist(pcoords, lcoords, 'euclidean'))
    # print(f"Dists: {dists}")
    # dist = dists.min()
    # print(f"Dist: {dist}")
    # print(f"dists took {time.time() - st}s")

    N = 4
    dist_candidates = []
    st_total = time.time()
    for n in range(N):
        st = time.time()
        # compute distances separately for each subset to stay within reasonable memory requirements
        dists = np.array(distance.cdist(pcoords, lcoords[n::N], 'euclidean'))
        dist_candidates.append(dists.min())
        print(f"dists phase {n+1}/{N} took {time.time() - st}s")
    dist = min(dist_candidates)

    print(f"dists total took {time.time() - st_total}s")
    print(f"Dist: {dist}")


    # # use voronoi diagram to compute minimum distance quicly
    # dist = shortest_dist_voronoi(np.array(lcoords), np.array(pcoords))


    # st = time.time()
    # # find lesion center of mass
    # lcom = np.array([lesion_features["center_of_mass_z_in_prostate"], lesion_features["center_of_mass_y_in_prostate"], lesion_features["center_of_mass_x_in_prostate"]])
    # # compute distances from prostate boundaries to lesion's center of mass
    # st = time.time()
    # dists_to_lcom = [norm(lcom - p) for p in pcoords]
    # print(f"1 took {time.time() - st}s")
    # # sort prostate coords according to dists_to_lcom to avoid as much computation as possible
    # idx = np.argsort(dists_to_lcom)
    # pcoords_sorted = np.array([pcoords[i] for i in idx])
    # dists_to_lcom_sorted = np.array([dists_to_lcom[i] for i in idx])

    # # find lesion maximum extent
    # st = time.time()
    # lesion_max_extent = max([norm(lcom - p) for p in np.array(lcoords)])
    # print(f"2 took {time.time() - st}s")

    # # loop through all prostate coordinates and compute distances to lesion coordinates
    # # skip computation for those prostate coordinates that are too far away to give minimum distance
    # min_dist = np.inf
    # comp_count = 0
    # skip_count = 0
    # for i, (p, d) in enumerate(zip(pcoords_sorted, dists_to_lcom_sorted)):
    #     # if some point on lesion's surface could possibly give shorter distance than current minimum, compute distances to all points
    #     if d - lesion_max_extent <= min_dist:
    #         comp_count = comp_count + 1

    #         # compute distances to all lesion's points
    #         dd = np.sqrt(np.sum((lcoords - p)**2, axis=1))

    #         # for l in lcoords:
    #         #     min_dist = np.min((norm(p - l), min_dist))

    #     else:
    #         skip_count = skip_count + 1

    # print(f"Skip count {comp_count}")
    # print(f"Skip count {skip_count}")
    # print(f"dists took {time.time() - st}s")

    return dist


def extract_tumor_mask_features(tumor: Volume, tumor_edge_volume: Volume,
                                tumor_dict: Dict):
    """
    Extract features from binary lesion masks and save them to tumor_dict
    Notice that tumor_dict is also the "output" variable
    """

    F = tumor_dict

    F["volume"] = \
        tumor.get_full_res_microns_between_slices() * \
        1 / tumor.get_scaling_coeff() * tumor.get_full_res_voxel_size_symmetric_um() * \
        1 / tumor.get_scaling_coeff() * tumor.get_full_res_voxel_size_symmetric_um() * \
        tumor.get_stack().sum()

    # F["surface_area"] = \
    #     tumor.get_full_res_microns_between_slices() * \
    #     1 / tumor.get_scaling_coeff() * tumor.get_full_res_voxel_size_symmetric_um() * \
    #     tumor_edge_volume.get_stack().sum()

    # compute average edge face area and multiply by number of voxels at volume edges
    F["surface_area"] = \
        tumor_edge_volume.get_stack().sum() * ((tumor.get_full_res_microns_between_slices() * (1 / tumor.get_scaling_coeff()) * tumor.get_full_res_voxel_size_symmetric_um()) + \
        (2*(1 / tumor.get_scaling_coeff()) * 2*tumor.get_full_res_voxel_size_symmetric_um())) / 2

    F["sphericity"] = F["volume"] / F["surface_area"]

    # length between section center features
    coms = [center_of_mass(im) for im in tumor.get_stack()]
    coms = [(i * tumor.get_full_res_microns_between_slices(),
             tumor.get_full_res_voxel_size_symmetric_um() * com[0],
             tumor.get_full_res_voxel_size_symmetric_um() * com[1])
            for i, com in enumerate(coms)]
    coms = np.array(coms)
    diff = np.diff(coms, axis=0)
    lengths = [norm(x) for x in diff]
    length_adjacent = sum(lengths)
    F["dist_section_center_adj_sum"] = length_adjacent
    length_endpoints = norm(coms[-1] - coms[0])
    F["dist_section_center_endpoints"] = length_endpoints
    F["dist_section_center_straightness"] = length_endpoints / length_adjacent

    feature_statistics(F, lengths, "dist_section_center")
    # feature_statistics(F, np.abs(np.diff(lengths)), "dist_section_center_diff")

    # pca features
    pca = PCA(n_components=3)
    coords = np.array(tumor.get_volume().nonzero()).T
    out = pca.fit_transform(coords)
    F["length_pca1"] = (out[:, 0].max() - out[:, 0].min()
                        ) * tumor.get_full_res_voxel_size_symmetric_um() * (
                            1 / tumor.get_scaling_coeff())
    F["length_pca2"] = (out[:, 1].max() - out[:, 1].min()
                        ) * tumor.get_full_res_voxel_size_symmetric_um() * (
                            1 / tumor.get_scaling_coeff())
    F["length_pca3"] = (out[:, 2].max() - out[:, 2].min()
                        ) * tumor.get_full_res_voxel_size_symmetric_um() * (
                            1 / tumor.get_scaling_coeff())
    F["length_pca_21_ratio"] = F["length_pca2"] / F["length_pca1"]
    F["length_pca_31_ratio"] = F["length_pca3"] / F["length_pca1"]
    F["length_pca_32_ratio"] = F["length_pca3"] / F["length_pca2"]

    # F["length_pca1_scaled"] = ((out[:, 0].max() - out[:, 0].min()
    #                     ) * tumor.get_full_res_voxel_size_symmetric_um() * (
    #                         1 / tumor.get_scaling_coeff())) / F["length_pca1"]  # this should be always one
    F["length_pca2_scaled"] = ((out[:, 1].max() - out[:, 1].min()
                        ) * tumor.get_full_res_voxel_size_symmetric_um() * (
                            1 / tumor.get_scaling_coeff())) / F["length_pca1"]
    F["length_pca3_scaled"] = ((out[:, 2].max() - out[:, 2].min()
                        ) * tumor.get_full_res_voxel_size_symmetric_um() * (
                            1 / tumor.get_scaling_coeff())) / F["length_pca1"]

    # get distances from each voxel to the 1st pca component
    pca1_axis_to_voxel_vectors = get_vectors_from_axis_to_voxels(pca.components_[0, :], coords)
    pca2_axis_to_voxel_vectors = get_vectors_from_axis_to_voxels(pca.components_[1, :], coords)
    pca3_axis_to_voxel_vectors = get_vectors_from_axis_to_voxels(pca.components_[2, :], coords)
    # remove mean to make axis travel through lesion's center of mass
    pca1_axis_to_voxel_vectors_throughcenter = pca1_axis_to_voxel_vectors - pca1_axis_to_voxel_vectors.mean(axis=0)
    pca2_axis_to_voxel_vectors_throughcenter = pca2_axis_to_voxel_vectors - pca2_axis_to_voxel_vectors.mean(axis=0)
    pca3_axis_to_voxel_vectors_throughcenter = pca3_axis_to_voxel_vectors - pca3_axis_to_voxel_vectors.mean(axis=0)

    # compute distances from voxels to pca axes
    # pca1_axis_distances = np.array([norm(x) for x in pca1_axis_to_voxel_vectors])  # don't use these, not scaled, not through center
    # pca2_axis_distances = np.array([norm(x) for x in pca2_axis_to_voxel_vectors])  # don't use these
    # pca3_axis_distances = np.array([norm(x) for x in pca3_axis_to_voxel_vectors])  # don't use these
    pca1_axis_distances_throughcenter = np.array([norm(x) for x in pca1_axis_to_voxel_vectors_throughcenter])
    pca2_axis_distances_throughcenter = np.array([norm(x) for x in pca2_axis_to_voxel_vectors_throughcenter])
    pca3_axis_distances_throughcenter = np.array([norm(x) for x in pca3_axis_to_voxel_vectors_throughcenter])
    # scale!
    # pca1_axis_distances_throughcenter /= np.std(pca1_axis_distances_throughcenter)
    # pca2_axis_distances_throughcenter /= np.std(pca2_axis_distances_throughcenter)
    # pca3_axis_distances_throughcenter /= np.std(pca3_axis_distances_throughcenter)

    feature_statistics(F, pca1_axis_distances_throughcenter, "dist_pca1_axis")
    feature_statistics(F, pca2_axis_distances_throughcenter, "dist_pca2_axis")
    feature_statistics(F, pca3_axis_distances_throughcenter, "dist_pca3_axis")

    # # moment of inertia w.r.t. pca axes, scale with lesion size, THIS IS CRAP
    # F["pca1_moment_of_inertia_size_normalized"] = (pca1_axis_distances_throughcenter**2).sum() / len(pca1_axis_distances_throughcenter)
    # F["pca2_moment_of_inertia_size_normalized"] = (pca2_axis_distances_throughcenter**2).sum() / len(pca2_axis_distances_throughcenter)
    # F["pca3_moment_of_inertia_size_normalized"] = (pca3_axis_distances_throughcenter**2).sum() / len(pca3_axis_distances_throughcenter)

    # moment of inertia w.r.t. pca axes
    # THIS IS POORLY SCALED
    F["pca1_moment_of_inertia"] = (pca1_axis_distances_throughcenter**2).sum() / F["volume"]  # volume is equivalent to mass here, so dividing by it -> get measure of shape
    F["pca2_moment_of_inertia"] = (pca2_axis_distances_throughcenter**2).sum() / F["volume"]
    F["pca3_moment_of_inertia"] = (pca3_axis_distances_throughcenter**2).sum() / F["volume"]

    # THESE ARE IMPROVEMENTS THAT WERE NOT USED IN THE PUBLICATION
    ## sum of distances from voxels to pca axes that traverse center of mass
    #F["pca1_sum_dists_per_volume"] = (pca1_axis_distances_throughcenter).sum() / F["volume"]  # volume is equivalent to mass here, so dividing by it -> get measure of shape
    #F["pca2_sum_dists_per_volume"] = (pca2_axis_distances_throughcenter).sum() / F["volume"]
    #F["pca3_sum_dists_per_volume"] = (pca3_axis_distances_throughcenter).sum() / F["volume"]
    # 
    ## get radius of sphere with equal volume
    #equal_vol_radius = np.cbrt(3 * F["volume"] / 4 * np.pi)
    ## moment of inertia for a sphere with equal volume
    ## (volume here equals mass since all voxels have mass 1)
    #equal_vol_moi = 2 / 5 * F["volume"] * equal_vol_radius**2
    # 
    ## moment of inertia w.r.t. pca axes
    #F["pca1_moment_of_inertia_vanilla"] = (pca1_axis_distances_throughcenter**2).sum()
    #F["pca2_moment_of_inertia_vanilla"] = (pca2_axis_distances_throughcenter**2).sum()
    #F["pca3_moment_of_inertia_vanilla"] = (pca3_axis_distances_throughcenter**2).sum()
    # 
    #F["pca1_moment_of_inertia_scaled_sphere"] = (pca1_axis_distances_throughcenter**2).sum() / equal_vol_moi
    #F["pca2_moment_of_inertia_scaled_sphere"] = (pca2_axis_distances_throughcenter**2).sum() / equal_vol_moi
    #F["pca3_moment_of_inertia_scaled_sphere"] = (pca3_axis_distances_throughcenter**2).sum() / equal_vol_moi

    # pca_weighted_components = pca.components_ * pca.explained_variance_ratio_
    # F["pca_weighted_component1_z"] = pca_weighted_components[0][0]
    # F["pca_weighted_component1_y"] = pca_weighted_components[0][1]
    # F["pca_weighted_component1_x"] = pca_weighted_components[0][2]
    # F["pca_weighted_component2_z"] = pca_weighted_components[1][0]
    # F["pca_weighted_component2_y"] = pca_weighted_components[1][1]
    # F["pca_weighted_component2_x"] = pca_weighted_components[1][2]
    # F["pca_weighted_component3_z"] = pca_weighted_components[2][0]
    # F["pca_weighted_component3_y"] = pca_weighted_components[2][1]
    # F["pca_weighted_component3_x"] = pca_weighted_components[2][2]

    # center of mass in tumor coordinate system (within bounding cube)
    com = compute_center_of_mass(tumor)
    # center fo mass in prostate cordinate system ::2 indexes all the min coordinates
    com_global = np.array(tumor.get_bounding_cube_coords())[::2] + np.array(com)
    F["center_of_mass_z_in_prostate"] = com_global[0]
    F["center_of_mass_y_in_prostate"] = com_global[1]
    F["center_of_mass_x_in_prostate"] = com_global[2]

    # distances from center of mass to all tumor voxels
    dists = np.array([norm(a) for a in com - coords])

    # moment of inertia, free rotation in every direction
    F["moment_of_inertia"] = (1 / tumor.get_scaling_coeff()
                              * tumor.get_full_res_voxel_size_symmetric_um()
                              * dists.sum()) / F["volume"]

    # # actual moment of inertia includes the sum of all mass elements, here average is computed
    # F["moment_of_inertia_average"] = (1 / tumor.get_scaling_coeff()
    #                                   * tumor.get_full_res_voxel_size_symmetric_um()
    #                                   * dists.sum() / coords.shape[0])

    # F["moment_of_inertia_size_normalized"] = (1 / tumor.get_scaling_coeff()
    #                           * tumor.get_full_res_voxel_size_symmetric_um()
    #                           * dists.sum()) / len(dists)

    feature_statistics(F, dists, "dist_to_tumor_center")

    # bounding cube features
    bc_coords = tumor.get_bounding_cube_coords()
    bc_dim_lengths = np.array([
        bc_coords[1] - bc_coords[0],
        bc_coords[3] - bc_coords[2],
        bc_coords[5] - bc_coords[4]
    ])
    F["bounding_cube_volume"] = np.prod(bc_dim_lengths)
    F["bounding_cube_diagonal_length"] = np.sqrt(np.sum(bc_dim_lengths**2))
    F["bounding_cube_x_to_diag_ratio"] = bc_dim_lengths[2] / F["bounding_cube_diagonal_length"]
    F["bounding_cube_y_to_diag_ratio"] = bc_dim_lengths[1] / F["bounding_cube_diagonal_length"]
    F["bounding_cube_z_to_diag_ratio"] = bc_dim_lengths[0] / F["bounding_cube_diagonal_length"]
    feature_statistics(F, bc_dim_lengths, "bounding_cube_dim")

    # convex hull features
    # ch = compute_convex_hull(tumor_edge_volume)
    ch = compute_convex_hull(tumor)
    F["convex_hull_surface_area"] = ch.area
    F["convex_hull_area_ratio"] = F["surface_area"] / ch.area
    F["convex_hull_volume"] = ch.volume
    F["solidity"] = F["volume"] / ch.volume

    # section features
    sect_perimeters = []
    for z, y, x, im in tumor_edge_volume.layer_iterator():
        sect_perimeters.append(im.sum())
    sect_perimeters = np.array(sect_perimeters)
    feature_statistics(F, sect_perimeters, "section_perimeter")
    feature_statistics(F, np.abs(np.diff(sect_perimeters)), "section_perimeter_adj_diff")
    F["section_perimeter_sum"] = sect_perimeters.sum()

    return F


def extract_features(tissue_volume: Volume,
                     tissue_edge_volume: Volume,
                     tumor_volumes: List[Volume],
                     tumor_edge_volumes: List[Volume],
                     mouse_id: str):

    logging.info("Extracting features..")
    # crate empty features data structure
    features = [dict() for i in range(len(tumor_volumes))]

    # 3d mask features
    for i, (t, te, f) in enumerate(zip(tumor_volumes, tumor_edge_volumes, features)):
        logging.info(f"    extracting shape features for lesion {i}")
        # insert mouse identifier into each dict
        f["mouse_id"] = mouse_id
        f["lesion_id"] = t.get_label()
        extract_tumor_mask_features(t, te, f)

    # distances to other tumors
    for i, (t, f) in enumerate(zip(tumor_volumes, features)):
        logging.info(f"    extracting distances-to-lesions features for lesion {i}")
        dists = [norm(np.array([f["center_of_mass_z_in_prostate"], f["center_of_mass_y_in_prostate"], f["center_of_mass_x_in_prostate"]]) - np.array([n["center_of_mass_z_in_prostate"], n["center_of_mass_y_in_prostate"], n["center_of_mass_x_in_prostate"]])) for n in features]

        # index 0 is the object itself -> distance = zero
        dists_sorted = np.sort(dists)[1:]
        f["dist_nearest_tumor"] = dists_sorted[0]
        f["dist_furthest_tumor"] = dists_sorted[-1]
        f["dist_tumor_average"] = dists_sorted.mean()
        f["dist_tumor_std"] = dists_sorted.std()

    # distances to landmarks
    # distance to prostate center of mass
    # pcom = tissue_volume.get_bounding_cube_coords()[::2] + compute_center_of_mass(tissue_volume)  # this is wrong
    pcom = compute_center_of_mass(tissue_volume)

    # get anatomical center in prostate coordinates
    # p_anatomical_center_dict = list(filter(lambda x: x["mouse_id"] == mouse_id, anatomical_centers))[0]
    p_anatomical_center_dict = list(filter(lambda x: x["mouse_id"].replace("-0", "-") == mouse_id.replace("-0", "-"), anatomical_centers))[0]
    fns = tissue_edge_volume.get_filenames()
    zinds = tissue_edge_volume._true_z_indices
    glass_nums = [re.findall("-([0-9]+)_", fn)[-1] for fn in fns]
    center_glass_num = re.findall("-([0-9]+)_", p_anatomical_center_dict["filename"])[-1]
    z = zinds[glass_nums.index(center_glass_num)]
    y = p_anatomical_center_dict["locy"]
    x = p_anatomical_center_dict["locx"]
    pac = np.array([z, y, x])

    for i, (t, te, f) in enumerate(zip(tumor_volumes, tumor_edge_volumes, features)):

        # logging.info(f"    extracting distances-to-center-of-mass features for lesion {i}")
        # tcom = np.array([f["center_of_mass_z_in_prostate"], f["center_of_mass_y_in_prostate"], f["center_of_mass_x_in_prostate"]])
        # f["dist_prostate_center"] = norm(pcom - tcom)

        logging.info(f"    extracting distances-to-prostate-anatomical-centers features for lesion {i}")
        tcom = np.array([f["center_of_mass_z_in_prostate"], f["center_of_mass_y_in_prostate"], f["center_of_mass_x_in_prostate"]])
        f["dist_prostate_anat_center_to_tumor_com"] = norm(pac - tcom)
        f["dist_prostate_anat_center_to_tumor_border"] = distance_between_tumor_and_point(te, pac)
        f["dist_prostate_border_to_tumor_border"] = distance_between_tumor_and_prostate_boundaries(te, tissue_edge_volume, f)

    # # distance to closest tumor boundary
    # for i, (t, f) in enumerate(zip(tumor_edge_volumes, features)):
    #     logging.info(f"    extracting distances-to-prostate-boundary features for lesion {i}")
    #     tcom = np.array([f["center_of_mass_z_in_prostate"], f["center_of_mass_y_in_prostate"], f["center_of_mass_x_in_prostate"]])
    #     coords = np.array(tissue_edge_volume.get_volume().nonzero()).T
    #     diff_coords = coords - tcom
    #     f["dist_prostate_boundary"] = np.array([norm(x) for x in diff_coords]).min()

    # convex hull features

    # # Extract heat kernel signature features
    # # convert to triangular mesh
    # for i, (t, f) in enumerate(zip(tumor_volumes, features)):
    #     logging.info(f"    extracting hks features for tumor {i}")
    #     verts, faces, normals, values = skimage.measure.marching_cubes_lewiner(t.get_volume(), level=0)
    #     # visualize mesh

    #     import pdb; pdb.set_trace()

    #     # Display resulting triangular mesh using Matplotlib. This can also be done
    #     fig = plt.figure(figsize=(10, 10))
    #     ax = fig.add_subplot(111, projection='3d')
    #     # Fancy indexing: `verts[faces]` to generate a collection of triangles
    #     mesh = Poly3DCollection(verts[faces])
    #     mesh.set_edgecolor('k')
    #     ax.add_collection3d(mesh)
    #     fig.savefig(f"test{i}.png")
    #     fig.close()

    #     # K = num eigenvalues to use, default 200
    #     hks = getHKS(verts, faces, K=10, ts=np.array([10, 100]))

    # convert triangular mesh to format accepted by pyhks

    return features


def compute_convex_hull(vol: Volume, decimate: int = 0):
    """Compute convex hull for all points in volume"""
    coords = []
    for z, y, x, im in vol.layer_iterator():
        ly, lx = np.nonzero(im)
        truey = y + ly
        truex = x + lx
        if decimate > 0:
            truey = truey[::decimate]
            truex = truex[::decimate]
        for yy, xx in zip(truey, truex):
            coords.append([z, yy, xx])
    ch = ConvexHull(coords)
    return ch


def plot_convex_hull_3d(vol: Volume, figure=None):

    ch = compute_convex_hull(vol)

    if figure is None:
        figure = plt.figure()
        ax = figure.add_subplot(111, projection='3d')
        ax.invert_zaxis()
    else:
        ax = figure.gca()

    # ax.scatter(ch.points[:, 2][::decimate], ch.points[:, 1][::decimate],
    #            ch.points[:, 0][::decimate])

    # plot simplices:
    # One simplex is a nD triangle defined by 3 points.
    # But the plotting function must cycle back to the last point,
    # otherwise only 2 of 3 simplex edges are drawn.
    # https://stackoverflow.com/questions/27270477/3d-convex-hull-from-point-cloud
    for s in ch.simplices:
        s = np.append(s, s[0])  # Here we cycle back to the first coordinate
        ax.plot(ch.points[s, 2], ch.points[s, 1], ch.points[s, 0], "r-")

    # # filled facets
    # for s in ch.simplices:
    #     s = np.append(s, s[0])  # Here we cycle back to the first coordinate
    #     # verts = list(zip(ch.points[s, 2], ch.points[s, 1], ch.points[s, 0]))
    #     verts = list(zip(ch.points[s, 0], ch.points[s, 1], ch.points[s, 2]))
    #     ax.add_collection3d(Poly3DCollection(verts))

    plt.draw()
    return figure


def compute_center_of_mass(vol: Volume):
    com = np.array(center_of_mass(vol.get_stack()))
    # now scale z to match reality
    com[0] = com[0] * vol.get_pixels_between_slices()
    return com


def load_images(path: str,
                image_regex: str = None,
                resize_percentage: float = None):
    # select images to load and sort them by sequence number
    tumor_fns = os.listdir(path)
    if image_regex:
        tumor_fns = filter_filelist_with_regex(image_regex, tumor_fns)
    sequence = extract_sequence_numbers_from_filenames(tumor_fns)
    sort_idx = np.argsort(sequence)
    tumor_fns = [tumor_fns[i] for i in sort_idx]

    # load images
    images = []
    for i, fn in enumerate(tumor_fns, start=1):
        impath = os.path.join(path, fn)
        im = imread(impath, flatten=True, plugin='imageio')

        logging.info("Loaded image {}/{} - shape {} - {}"
                     "".format(i, len(tumor_fns), 100 / resize_percentage,
                               im.shape, fn))

        print_mem_usage()
        images.append(im)

    return images, tumor_fns


def plot_volume_3d_scatter(vol: Volume,
                           decimate: int = 6,
                           figure=None,
                           color=None):

    # plot
    if figure is None:
        figure = plt.figure()
        ax = figure.add_subplot(111, projection='3d')
        ax.invert_zaxis()
    else:
        ax = figure.gca()

    for i, (z, y, x, im) in enumerate(vol.layer_iterator()):
        ly, lx = np.nonzero(im)

        truey = y + ly
        truex = x + lx

        truey = truey[::decimate]
        truex = truex[::decimate]

        logging.debug("Plotting layer: {} --- z: {} - y: {} - x: {}".format(
            i, z, truey, truex))
        ax.scatter(truex, truey, z, color=color)

    # plt.gca().invert_zaxis()
    figure.tight_layout()
    plt.xlabel("x")
    plt.ylabel("y")
    # plt.zlabel("z")
    plt.draw()
    return figure


def plot_stack_3d(stack: np.ndarray, decimate=6):
    # compute edges
    # edges = binary_dilation(stack) - stack
    # y, x, z = np.nonzero(edges)

    z, y, x = np.nonzero(stack)
    logging.info(stack.shape)

    # subsample
    y = y[::decimate]
    x = x[::decimate]
    z = z[::decimate]

    # plot
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(x, y, z)
    plt.gca().invert_zaxis()
    fig.tight_layout()

    plt.show()


def save_features(features: dict, output_path, delim: str = ","):
    logging.info("Saving features to {}".format(output_path))

    keys = features[0].keys()

    with open(output_path, 'w') as f:
        # write column names
        for key in keys:
            f.write(key + delim)
        f.write("\n")

        for F in features:
            for key in keys:
                f.write("{}{}".format(F[key], delim))
            f.write("\n")


def extract_edge_volumes(volumes: List[Volume]):
    # extract edges
    edge_volumes = []
    for vol in volumes:
        edge_stack = []
        for (z, y, x, image) in vol.layer_iterator():
            eroded = skimage.morphology.binary_erosion(image)
            bw_edge = image - eroded
            edge_stack.append(bw_edge)
        # let's create a new volume from edges only stack
        evol = Volume(np.array(edge_stack),
                      vol.get_scaling_coeff() * 100,
                      vol.get_bounding_cube_coords(), vol.get_filenames())
        edge_volumes.append(evol)
    return edge_volumes


def load_volumes(filenames, return_first_N: int = None):
    volumes = []
    for i, l in enumerate(filenames, start=1):
        logging.debug("Loading volume {}/{} - {}".format(i, len(filenames), l))
        vol = load_compressed_object(l)

        logging.info("Volume filenames:")
        for fn in vol.get_filenames():
            logging.info("   {}".format(fn))

        volumes.append(vol)
        if (return_first_N is not None) and (i == return_first_N):
            break
    return volumes


def main():
    start_timing()
    args = parse_arguments()
    check_arguments(args)
    logging.info("Command line arguments: {}".format(args))

    fns = os.listdir(args["volumes_dir"])
    fns = [os.path.join(args["volumes_dir"], p) for p in fns]
    logging.info("Volumes in volumes_dir: {}".format(fns))

    tumor_fns = list(filter(lambda x: x.find("Tumorvolume") != -1, fns))
    logging.info("Tumor volumes: {}".format(tumor_fns))
    tissue_fns = list(filter(lambda x: x.find("Tissuevolume") != -1, fns))
    logging.info("Tissue volumes: {}".format(tissue_fns))

    tissue_volumes = load_volumes(tissue_fns, return_first_N=1)
    tumor_volumes = load_volumes(tumor_fns)

    logging.info("Extracting features from tumor volumes with labels: ")
    for i, vol in enumerate(tumor_volumes):
        logging.info(f"    volume {i} label: {vol.get_label()}")

    # sctterplot edges
    tissue_edge_volumes = extract_edge_volumes(tissue_volumes)
    tumor_edge_volumes = extract_edge_volumes(tumor_volumes)

    # if matplotlib.get_backend() != "Agg":

    #     fig = plt.figure()
    #     ax = fig.add_subplot(111, projection='3d')
    #     ax.invert_zaxis()

    #     fig = plot_volume_3d_scatter(tissue_edge_volumes[0],
    #                                  color='b',
    #                                  decimate=120,
    #                                  figure=fig)

    #     # plot all tumors
    #     for tvol in tumor_edge_volumes:
    #         fig = plot_volume_3d_scatter(tvol, color='g', decimate=30, figure=fig)
    #         fig = plot_convex_hull_3d(tvol, figure=fig)
    #     plt.show()

    mouse_id = re.findall("/(PTENX[^/]*)/", tumor_fns[0])[0]
    features = extract_features(tissue_volumes[0], tissue_edge_volumes[0], tumor_volumes, tumor_edge_volumes, mouse_id)
    save_features(features, args["output_csv"])

    end_timing()


if __name__ == '__main__':
    main()
