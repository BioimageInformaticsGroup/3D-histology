import sys
import datetime
import logging
import subprocess
from time import sleep
from typing import List

import numpy as np

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

SQUEUE_POLLING_INTERVAL = 10  # seconds


# TODO: Make a class

class SlurmStatus:
    """Class to hold information about ongoing jobs in slurm queue"""
    jobs = []

    def add_jobs(self, job_ids: List[int]):
        pass

    def remove_jobs(self, job_ids: List[int]):
        pass


status = SlurmStatus()


def get_username():
    """Return executer's username"""
    c1 = subprocess.run(["whoami"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return c1.stdout[:-1].decode("utf-8")

username = get_username()


def get_user_slurm_queue(username: str = None) -> List[int]:
    """Return current slurm queue for the specified user"""

    if not username:
        username = get_username()

    command = ["/usr/bin/squeue", "-u", username]
    logger.debug("subprocess.run: {}".format(" ".join(command)))
    c2 = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="utf-8", bufsize=1)

    if c2.stderr != "":
        logger.debug("Subprocess STDERR: '{}'".format(c2.stderr))

    lines = c2.stdout.split("\n")[1:]
    lines = [s.strip() for s in lines]
    lines = [list(filter(None, l.split(" "))) for l in lines]
    lines = list(filter(None, lines))
    return lines


def cancel_job_ids(job_ids: List[int]):
    """Return current slurm queue for the specified user"""
    command = ["/usr/bin/scancel"]
    command.extend([str(jid) for jid in job_ids])
    logger.debug("subprocess.run: {}".format(" ".join(command)))
    c2 = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="utf-8", bufsize=1)

    if c2.stderr != "":
        logger.debug("Subprocess STDERR: '{}'".format(c2.stderr))


def parse_job_id(sbatch_output: str):
    """
    Extract job_id from the sbatch output
    Args:
        sbatch_output: Output of sbatch command containing job_id
            example: 'Submitted batch job 2987194'
    Returns:
        job_id
            example: 2987194
    """
    return sbatch_output.replace("\n", "").split(" ")[3]


def parse_job_ids(slurm_queue_list: List[str]):
    """Parse job_ids form the output of get_user_slurm_queue()"""
    return [l[0] for l in slurm_queue_list]


def wait_for_ids(job_ids: List[int]) -> None:
    """
    This function blocks execution until all specified jobs have finished

    Args:
        job_ids: List of slurm job ids
    """

    username = get_username()

    logger.info(
        "Begin waiting for {} job ids, squeue polling interval {}s".format(len(job_ids), SQUEUE_POLLING_INTERVAL))

    start_time = datetime.datetime.now()
    cont = True
    while cont:
        cont = False
        queue = get_user_slurm_queue(username)
        queue_ids = [l[0] for l in queue]

        logger.debug("Jobs currently being waited for: {}".format(job_ids))
        logger.debug("Jobs in queue: {}".format(queue_ids))

        # count number of unfinished jobs of interest
        counter = 0
        for jid in job_ids:
            if jid in queue_ids:
                counter += 1
                cont = True

        elapsed = datetime.datetime.now() - start_time

        if cont:

            line = "Waiting for {0} jobs to finish ({1}/{2}) - " \
                   "Time elapsed {3}d {4}h {5}m {6}s \r".format(counter,
                                                                len(job_ids) - counter,
                                                                len(job_ids),
                                                                elapsed.days,
                                                                int(np.floor(elapsed.seconds / (60 * 60))),
                                                                int(np.floor(elapsed.seconds / 60)),
                                                                elapsed.seconds % 60)

            logger.debug(line)
            sys.stdout.write(line)
            sys.stdout.flush()
            sleep(SQUEUE_POLLING_INTERVAL)
    logger.info("Waiting done for {} jobs".format(len(job_ids)))


def get_sbatch_script_header(slurm_job_name: str,
                             slurm_log_file: str,
                             slurm_time: str,
                             slurm_partition: str,
                             slurm_ntasks: int = None,
                             slurm_cpus_per_task: int = None,
                             slurm_mem: int = None,
                             slurm_mem_per_cpu: int = None) -> List[str]:
    """Creates sbatch script header with the given arguments"""

    # TODO: Check arguments

    template = [
        "#!/usr/bin/env bash",
        "#SBATCH -J {}".format(slurm_job_name),
        "#SBATCH --output={}".format(slurm_log_file),
        "#SBATCH --error={}".format(slurm_log_file),
        "#SBATCH --time={}".format(slurm_time),
        "#SBATCH --partition={}".format(slurm_partition)
    ]

    # append template with given optional parameters
    if slurm_ntasks:
        template.append("#SBATCH --ntasks={}".format(slurm_ntasks))
    if slurm_cpus_per_task:
        template.append("#SBATCH --cpus-per-task={}".format(slurm_cpus_per_task))
    if slurm_mem:
        template.append("#SBATCH --mem={}".format(slurm_mem))
    if slurm_mem_per_cpu:
        template.append("#SBATCH --mem-per-cpu={}".format(slurm_mem_per_cpu))

    return template


def send_sbatch_script(script: List[str]):
    """Send sbatch script to the slurm job scheduling system using 'sbatch' command"""
    script = "\n".join(script)

    command = "sbatch"
    logger.debug("subprocess.run: {} with stdin: \n{}".format(command, script))
    c2 = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, input=script, encoding="utf-8")
    logger.debug("subprocess.run output: {}".format(vars(c2)))
    job_id = parse_job_id(c2.stdout)
    logger.debug("parsed job_id: '{}'".format(job_id))
    return job_id


def wait_until_N_or_less_jobs_in_queue(n: int):
    """Wait until there are N or less jobs in slurm queue"""
    logger.info("Waiting until there are {} or less jobs in queue".format(n))
    queue = get_user_slurm_queue(username)
    logger.debug("Queue: {}".format(queue))
    wait_amount = 30
    while len(queue) > n:
        logger.debug("Waiting for {} seconds. {} > {}".format(wait_amount, len(queue), n))
        sleep(wait_amount)
        queue = get_user_slurm_queue(username)
    logger.info("waiting done".format(n))
    return

