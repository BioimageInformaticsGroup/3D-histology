import logging
from collections import OrderedDict

# setup logging
from cli.settings import CONSOLE_LOG_LEVEL

logger = logging.getLogger(__name__)
logger.setLevel(CONSOLE_LOG_LEVEL)


class EsaParams():
    params = None

    def __init__(self):
        self.params = self.default()

    def default(self):

        logger.info("Using DEFAULT parameters")

        p = OrderedDict(
            [("imagepixelsize", 0.46),
             ("rod", 0.95),
             ("rejectidentity", False),
             ("identitytolerance", 0),
             ("maxepsilon", 561.4478),
             ("siftfdbins", 8),
             ("siftfdsize", 8),
             ("siftinitialsigma", 1.6),
             ("siftmaxoctavesize", 1024),
             ("siftminoctavesize", 32),
             ("siftsteps", 3),
             ("maxnumfailures", 1),
             ("maxnumneighbors", 2),  # vai 10?
             ("maxiterationsoptimize", 1508),
             ("maxplateauwidthoptimize", 302),
             ("layerscale", 0.015625),
             ("resolutionspringmesh", 31),
             ("minr", 0.6),
             ("maxcurvaturer", 7.00582628641793),
             ("rodr", 0.863975346789895),
             ("localmodelindex", 1),
             ("maxlocaltrust", 2.5798143212755),
             ("maxstretchspringmesh", 17728.04),
             ("dampspringmesh", 0.9),
             ("stiffnessspringmesh", 0.584399040198663),
             ("maxplateauwidthspringmesh", 302),
             ("maxiterationsspringmesh", 1508),
             ("uselegacyoptimizer", True),
             ("searchradius", 272),
             ("blockradius", 496),
             ("localregionsigma", 907.3362),
             ("maxlocalepsilon", 290.4741),
             ("sectionthickness", 50.0),
             ("clearcache", True),
             ("widestsetonly", False),
             ("multiplehypotheses", True),
             ("visualize", False),
             ("isaligned", False),
             ("uselocalsmoothnessfilter", True),
             ("maxnumthreadssift", 16),
             ("desiredmodelindex", 1),
             ("expectedmodelindex", 1),
             ("maxnumthreads", 16),
             ("mininlierratio", 0),
             ("minnuminliers", 3)]
        )
        return p

    def optimized_kimmo(self):

        logger.info("Using Kimmo's OPTIMIZED parameters")

        p = OrderedDict(
            [("imagepixelsize", 0.46),
             ("rod", 0.874208375277442),
             ("rejectidentity", False),
             ("identitytolerance", 0),
             ("maxepsilon", 561.4478),
             ("siftfdbins", 8),
             ("siftfdsize", 4),
             ("siftinitialsigma", 1.6),
             ("siftmaxoctavesize", 16384),
             ("siftminoctavesize", 32),
             ("siftsteps", 3),
             ("maxnumfailures", 1),
             ("maxnumneighbors", 10),
             ("maxiterationsoptimize", 1508),
             ("maxplateauwidthoptimize", 302),
             ("layerscale", 1.0),
             ("resolutionspringmesh", 31),
             ("minr", 0.723048369673122),
             ("maxcurvaturer", 7.00582628641793),
             ("rodr", 0.863975346789895),
             ("localmodelindex", 1),
             ("maxlocaltrust", 2.5798143212755),
             ("maxstretchspringmesh", 17728.04),
             ("dampspringmesh", 0.9),
             ("stiffnessspringmesh", 0.584399040198663),
             ("maxplateauwidthspringmesh", 302),
             ("maxiterationsspringmesh", 1508),
             ("uselegacyoptimizer", True),
             ("searchradius", 272),
             ("blockradius", 496),
             ("localregionsigma", 907.3362),
             ("maxlocalepsilon", 290.4741),
             ("sectionthickness", 10),
             ("clearcache", True),
             ("widestsetonly", False),
             ("multiplehypotheses", True),
             ("visualize", False),
             ("isaligned", False),
             ("uselocalsmoothnessfilter", True),
             ("maxnumthreadssift", 1),
             ("desiredmodelindex", 1),
             ("expectedmodelindex", 1),
             ("maxnumthreads", 1),
             ("mininlierratio", 0),
             ("minnuminliers", 3)]
        )
        return p

    def get_cli_arguments(self):
        arguments = str()
        arguments += "--imagepixelsize={} ".format(self.params["imagepixelsize"])
        arguments += "--rod={} ".format(self.params["rod"])
        arguments += "--rejectidentity={} ".format(self.params["rejectidentity"])
        arguments += "--identitytolerance={} ".format(self.params["identitytolerance"])
        arguments += "--maxepsilon={} ".format(self.params["maxepsilon"])
        arguments += "--siftfdbins={} ".format(self.params["siftfdbins"])
        arguments += "--siftfdsize={} ".format(self.params["siftfdsize"])
        arguments += "--siftinitialsigma={} ".format(self.params["siftinitialsigma"])
        arguments += "--siftmaxoctavesize={} ".format(self.params["siftmaxoctavesize"])
        arguments += "--siftminoctavesize={} ".format(self.params["siftminoctavesize"])
        arguments += "--siftsteps={} ".format(self.params["siftsteps"])
        arguments += "--maxnumfailures={} ".format(self.params["maxnumfailures"])
        arguments += "--maxnumneighbors={} ".format(self.params["maxnumneighbors"])
        arguments += "--maxiterationsoptimize={} ".format(self.params["maxiterationsoptimize"])
        arguments += "--maxplateauwidthoptimize={} ".format(self.params["maxplateauwidthoptimize"])
        arguments += "--layerscale={} ".format(self.params["layerscale"])
        arguments += "--resolutionspringmesh={} ".format(self.params["resolutionspringmesh"])
        arguments += "--minr={} ".format(self.params["minr"])
        arguments += "--maxcurvaturer={} ".format(self.params["maxcurvaturer"])
        arguments += "--rodr={} ".format(self.params["rodr"])
        arguments += "--localmodelindex={} ".format(self.params["localmodelindex"])
        arguments += "--maxlocaltrust={} ".format(self.params["maxlocaltrust"])
        arguments += "--maxstretchspringmesh={} ".format(self.params["maxstretchspringmesh"])
        arguments += "--dampspringmesh={} ".format(self.params["dampspringmesh"])
        arguments += "--stiffnessspringmesh={} ".format(self.params["stiffnessspringmesh"])
        arguments += "--maxplateauwidthspringmesh={} ".format(self.params["maxplateauwidthspringmesh"])
        arguments += "--maxiterationsspringmesh={} ".format(self.params["maxiterationsspringmesh"])
        arguments += "--uselegacyoptimizer={} ".format(self.params["uselegacyoptimizer"])
        arguments += "--searchradius={} ".format(self.params["searchradius"])
        arguments += "--blockradius={} ".format(self.params["blockradius"])
        arguments += "--localregionsigma={} ".format(self.params["localregionsigma"])
        arguments += "--maxlocalepsilon={} ".format(self.params["maxlocalepsilon"])
        arguments += "--sectionthickness={} ".format(self.params["sectionthickness"])
        arguments += "--clearcache={} ".format(self.params["clearcache"])
        arguments += "--widestsetonly={} ".format(self.params["widestsetonly"])
        arguments += "--multiplehypotheses={} ".format(self.params["multiplehypotheses"])
        arguments += "--visualize={} ".format(self.params["visualize"])
        arguments += "--isaligned={} ".format(self.params["isaligned"])
        arguments += "--uselocalsmoothnessfilter={} ".format(self.params["uselocalsmoothnessfilter"])
        arguments += "--maxnumthreadssift={} ".format(self.params["maxnumthreadssift"])
        arguments += "--desiredmodelindex={} ".format(self.params["desiredmodelindex"])
        arguments += "--expectedmodelindex={} ".format(self.params["expectedmodelindex"])
        arguments += "--maxnumthreads={} ".format(self.params["maxnumthreads"])
        arguments += "--mininlierratio={} ".format(self.params["mininlierratio"])
        arguments += "--minnuminliers={} ".format(self.params["minnuminliers"])
        return arguments
