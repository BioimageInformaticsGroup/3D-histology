import argparse
import os
import re
import sys
import time
from typing import List

import psutil


def extract_sequence_numbers_from_filenames(filenames: List[str]):
    """ Extract sequence number / slice index from list of filenames.
    Sequence number is the running number (usually between 1-99) in a slice filename """
    return [int(re.findall("-([0-9]*)_", x, flags=0)[-1]) for x in filenames]


def extract_mouse_identifier(filename: str):
    """Extract mouse identifier from a single filename"""
    match = re.search("((Lat|LAT)\-.*?)\-[0-9]+?_", filename)
    string = match.groups()[0]
    return string


def filter_filelist_with_regex(pattern: str, fnames: List[str]):
    matches = [re.findall(".*{}.*".format(pattern), f) for f in fnames]
    out = [f[0] for f in filter(None, matches)]
    return out


def print_mem_usage():
    process = psutil.Process(os.getpid())
    # output in megabytes
    print("Memory usage: {:.2f} MiB".format(process.memory_info().rss / (1024 * 1024)))
    sys.stdout.flush()


def get_mem_usage():
    process = psutil.Process(os.getpid())
    # output in megabytes
    usage = "{:.2f} MiB".format(process.memory_info().rss / (1024 * 1024))
    return usage


def start_timing():
    global start_time
    start_time = time.time()


def end_timing():
    global start_time
    elapsed = time.time() - start_time
    exec_time = time.strftime("%H:%M:%S", time.gmtime(elapsed))
    print("Execution finished in {}".format(exec_time))


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')
