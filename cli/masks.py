import copy
import logging
import os

from biit_slurm import slurm
from biit_slurm.slurm import wait_for_ids
from cli.settings import CONSOLE_LOG_LEVEL, FIJI_PATH, REAPPLY_PATH, ACTIVATE_PATH, ENV_PATH, PREPROCESS_PATH, \
    POSTPROCESS_PATH

logger = logging.getLogger(__name__)
logger.setLevel(CONSOLE_LOG_LEVEL)

PREPROCESSED_TISSUE_DIR = "preprocessed_tissuemasks"
REAPPLIED_TISSUE_DIR = "reapplied_tissuemasks"
PREPROCESSED_TUMOR_DIR = "preprocessed_tumormasks"
REAPPLIED_TUMOR_DIR = "reapplied_tumormasks"


def check_preprocess_arguments(args):
    if not os.path.exists(args["output_dir"]):
        logger.debug("Output directory {} does not exist, creating it".format(args["output_dir"]))
        os.makedirs(args["output_dir"])


def preprocess(args):
    logger.info("Starting mask preprocessing")
    check_preprocess_arguments(args)

    id = preprocess_sbatch(args["masks_dir"],
                           args["output_dir"],
                           log_file=args["log_file"],
                           partition=args["partition"],
                           max_time=args["time"],
                           invert=args["invert"])

    return id


def preprocess_sbatch(input_directory: str,
                      output_directory: str,
                      invert: bool = False,
                      job_name: str = "H_prep",
                      log_file: str = "prep.log",
                      ntasks: int = 1,
                      cpus_per_task: int = 8,
                      max_time: str = "3:59:00",
                      mem: int = 100000,
                      partition: str = "test"):
    """ Run preprocess_masks.py as Slurm job with the given parameters """

    script = slurm.get_sbatch_script_header(slurm_job_name=job_name,
                                            slurm_log_file=log_file,
                                            slurm_ntasks=ntasks,
                                            slurm_cpus_per_task=cpus_per_task,
                                            slurm_time=max_time,
                                            slurm_mem=mem,
                                            slurm_partition=partition)

    # activate conda environment
    script.append("source {} {}".format(ACTIVATE_PATH, ENV_PATH))
    # log which python is being used
    script.append("echo 'Python interpreter: $(which python)'")
    invert_arg = "--invert" if invert else ""
    # execute preprocessing
    script.append("python {} {} {} {}".format(PREPROCESS_PATH, input_directory, output_directory, invert_arg))
    # send created job for execution on Slurm
    job_id = slurm.send_sbatch_script(script)
    return job_id


def check_reapply_arguments(args):
    if not os.path.exists(args["output_dir"]):
        logger.debug("Output directory {} does not exist, creating it".format(args["output_dir"]))
        os.makedirs(args["output_dir"])


def reapply(args):
    logger.info("Starting reapply transform")
    check_reapply_arguments(args)

    id = reapply_sbatch(args["source_dir"],
                        args["target_dir"],
                        args["projectfile_path"],
                        log_file=args["log_file"],
                        partition=args["partition"],
                        max_time=args["time"],
                        grayscale=args["grayscale"],
                        modify_project_file=args["modify_project_file"],
                        work_dir=args["work_dir"])

    return id


def reapply_sbatch(source_dir: str,
                   target_dir: str,
                   projectfile_path: str,
                   job_name: str = "H_reapply",
                   log_file: str = "reapply.log",
                   ntasks: int = 1,
                   cpus_per_task: int = 8,
                   max_time: str = "3:59:00",
                   mem: int = 100000,
                   partition: str = "test",
                   grayscale: bool = False,
                   modify_project_file: bool = False,
                   work_dir: str = None,
                   save_template: str = "Transformed_{}.png"):
    """ Run reapply_transform.py as Slurm job with the given parameters """

    script = slurm.get_sbatch_script_header(slurm_job_name=job_name,
                                            slurm_log_file=log_file,
                                            slurm_ntasks=ntasks,
                                            slurm_cpus_per_task=cpus_per_task,
                                            slurm_time=max_time,
                                            slurm_mem=mem,
                                            slurm_partition=partition)

    arguments = ""
    if grayscale:
        arguments += " --grayscale"
    if modify_project_file:
        arguments += " --modify-project-file"
    if save_template:
        arguments += " --save-template={}".format(save_template)
    if work_dir is not None:
        arguments += " --work-dir={}".format(work_dir)

    script.append("{} --headless -- {} {} {} {} {}".format(FIJI_PATH,
                                                           REAPPLY_PATH,
                                                           source_dir,
                                                           target_dir,
                                                           projectfile_path,
                                                           arguments))

    job_id = slurm.send_sbatch_script(script)
    return job_id


def check_postprocess_arguments(args):
    if not os.path.exists(args["output_dir"]):
        logger.debug("Output directory {} does not exist, creating it".format(args["output_dir"]))
        os.makedirs(args["output_dir"])


def postprocess(args):
    logger.info("Starting mask postprocessing")
    check_postprocess_arguments(args)

    id = postprocess_sbatch(args["masks_dir"],
                            args["images_dir"],
                            args["output_dir"],
                            mem=100000,
                            log_file=args["log_file"],
                            partition=args["partition"],
                            max_time=args["time"])

    return id


def postprocess_sbatch(masks_path: str,
                       images_path: str,
                       output_path: str,
                       job_name: str = "H_postp",
                       log_file: str = "postp.log",
                       ntasks: int = 1,
                       cpus_per_task: int = 1,
                       max_time: str = "3:00:00",
                       mem: int = 254000,
                       partition: str = "normal",
                       image_regex: str = "",
                       mask_regex: str = "",
                       save_template: str = "",
                       upscale: str = "",
                       create_only_empties: str = ""):
    """ Run postprocess_masks.py as Slurm job with the given parameters """

    script = slurm.get_sbatch_script_header(slurm_job_name=job_name,
                                            slurm_log_file=log_file,
                                            slurm_ntasks=ntasks,
                                            slurm_cpus_per_task=cpus_per_task,
                                            slurm_time=max_time,
                                            slurm_mem=mem,
                                            slurm_partition=partition)

    # activate conda environment
    script.append("source {} {}".format(ACTIVATE_PATH, ENV_PATH))
    # log which python environment is being used
    # script.append("conda env list")
    script.append("which python")
    # execute postprocessing
    script.append("python {} {} {} {}".format(POSTPROCESS_PATH, masks_path, images_path, output_path))
    # send created job for execution on Slurm
    job_id = slurm.send_sbatch_script(script)
    return job_id


def process_masks_tumor(args):
    logger.info("Begin mask processing")

    preprocess_args = copy.deepcopy(args)
    reapply_args = copy.deepcopy(args)
    postprocess_args = copy.deepcopy(args)

    inter_1 = os.path.join(args["intermediate_dir"], PREPROCESSED_TUMOR_DIR)
    inter_2 = os.path.join(args["intermediate_dir"], REAPPLIED_TUMOR_DIR)

    # preprocess
    preprocess_args["images_dir"] = args["images_dir"]
    preprocess_args["output_dir"] = inter_1
    preprocess_args["log_file"] = "preprocess_tumor.log"
    preprocess_ids = preprocess(preprocess_args)
    wait_for_ids([preprocess_ids])

    # reapply transform
    reapply_args["source_dir"] = inter_1
    reapply_args["target_dir"] = inter_2
    reapply_args["projectfile_path"] = args["projectfile_path"]
    reapply_args["grayscale"] = args["grayscale"]
    reapply_args["modify_project_file"] = args["modify_project_file"]
    reapply_args["log_file"] = "reapply_tumor.log"
    reapply_args["work_dir"] = None
    # reapply_args["save_template"] = args["save_template"]
    reapply_ids = reapply(reapply_args)
    wait_for_ids([reapply_ids])

    # postprocess
    postprocess_args["masks_dir"] = inter_2
    postprocess_args["images_dir"] = args["images_dir"]
    postprocess_args["output_dir"] = args["output_dir"]
    postprocess_args["log_file"] = "postprocess_tumor.log"

    # some arguments are missing here, add if needed
    postprocess_ids = postprocess(postprocess_args)
    wait_for_ids([postprocess_ids])


def process_masks_tissue(args):
    logger.info("Begin mask processing")

    # preprocess_args = copy.deepcopy(args)
    reapply_args = copy.deepcopy(args)
    # postprocess_args = copy.deepcopy(args)

    # inter_1 = os.path.join(args["intermediate_dir"], PREPROCESSED_TISSUE_DIR)
    # inter_2 = os.path.join(args["intermediate_dir"], REAPPLIED_TISSUE_DIR)

    # # preprocess
    # preprocess_args["images_dir"] = args["images_dir"]
    # preprocess_args["output_dir"] = inter_1
    # preprocess_args["log_file"] = "preprocess_tissue.log"
    # # skip preprocessing for tissuemasks, no need to invert
    # preprocess_ids = preprocess(preprocess_args)
    # wait_for_ids([preprocess_ids])

    # reapply transform
    reapply_args["source_dir"] = args["masks_dir"]
    reapply_args["images_dir"] = args["images_dir"]
    reapply_args["target_dir"] = args["output_dir"]
    reapply_args["projectfile_path"] = args["projectfile_path"]
    reapply_args["grayscale"] = args["grayscale"]
    reapply_args["modify_project_file"] = args["modify_project_file"]
    reapply_args["log_file"] = "reapply_tissue.log"
    reapply_args["work_dir"] = None
    # reapply_args["save_template"] = args["save_template"]
    reapply_ids = reapply(reapply_args)
    wait_for_ids([reapply_ids])


# def process_masks_tissue(args):
#     logger.info("Begin mask processing")

#     preprocess_args = copy.deepcopy(args)
#     reapply_args = copy.deepcopy(args)
#     postprocess_args = copy.deepcopy(args)

#     inter_1 = os.path.join(args["intermediate_dir"], PREPROCESSED_TISSUE_DIR)
#     inter_2 = os.path.join(args["intermediate_dir"], REAPPLIED_TISSUE_DIR)

#     # preprocess
#     preprocess_args["images_dir"] = args["images_dir"]
#     preprocess_args["output_dir"] = inter_1
#     preprocess_args["log_file"] = "preprocess_tissue.log"
#     # skip preprocessing for tissuemasks, no need to invert
#     preprocess_ids = preprocess(preprocess_args)
#     wait_for_ids([preprocess_ids])

#     # reapply transform
#     reapply_args["source_dir"] = inter_1
#     reapply_args["target_dir"] = args["output_dir"]
#     reapply_args["projectfile_path"] = args["projectfile_path"]
#     reapply_args["grayscale"] = args["grayscale"]
#     reapply_args["modify_project_file"] = args["modify_project_file"]
#     reapply_args["log_file"] = "reapply_tissue.log"
#     reapply_args["work_dir"] = None
#     # reapply_args["save_template"] = args["save_template"]
#     reapply_ids = reapply(reapply_args)
#     wait_for_ids([reapply_ids])

