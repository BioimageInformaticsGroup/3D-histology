function visualize_tissue_3D(inputpath,ext,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity)
% visualize_tissue_3D - Visualize tissue stack in 3D.
%
%   visualize_tissue_3D(inputpath,pixelsize,slicethickness,zscaling,reductionlevel,opacity)
%   Visualizes a reconstructed 3D volume. Requires vol3d.m in path.
%
%   'inputpath' is the path to the image files.
%
%   'ext' is the file extension (e.g. '.tif').
%
%   'fullrespixelsize' is the original size of pixels in XY-plane (e.g. 0.46).
%
%   'slicethickness' is the section spacing in Z direction (e.g. 5).
%
%   'zscaling' is the amount of scaling to apply in Z direction.
%
%   'reductionlevel' is the resampling factor to apply to the
%   images (e.g. 2 corresponds to 2^2 = 4-fold subsampling).
%
%   'opacity' is the amount of opacity (0-1) applied to the tissue.

addpath(genpath('../biit-wsi-featureextraction'));


% Scale pixel size.
resamplingfactorXY = 1/(2^reductionlevel);
pixelsize = fullrespixelsize/resamplingfactorXY;

% Get sorted filenames for images and read them.
filenames = dir([inputpath,filesep,'*',ext]);
filenames = {filenames.name};
numimages = length(filenames);
slidenumber = cell(numimages,1);
% Get the last number(if multiple exist) in filename that is between - and _
for imind = 1:numimages
    startinds = regexp(filenames{imind}, '-[0-9]+_', 'start');
    endinds = regexp(filenames{imind}, '-[0-9]+_', 'end');
    slidenumber{imind} = filenames{imind}(startinds(end)+1:endinds(end)-1);
end
slidenumber = cellfun(@str2num,slidenumber);
% sort filenames according to slide number
[~,ind] = sort(slidenumber);
filenames = filenames(ind);


% Read images, resampling in XY if required.
info = imfinfo([inputpath,filesep,filenames{1}]);
imsize = [round(info(1).Height*resamplingfactorXY) round(info(1).Width*resamplingfactorXY)];
images = zeros(imsize(1),imsize(2),numimages,3,'uint8');
parfor imind = 1:numimages
    tmp = biitwsi_imread([inputpath,filesep,filenames{imind}],'ReductionLevel',floor(reductionlevel));
    tmp = imresize(tmp,[imsize(1) imsize(2)],'nearest');
    % convert black areas to white
    isblack = tmp(:,:,1) == 0 & tmp(:,:,2) == 0 & tmp(:,:,3) == 0;
%     isblack = imdilate(isblack,strel('square',round(200*resamplingfactorXY)));
    tmp(repmat(isblack,1,1,3)) = 255;
    images(:,:,imind,:) = tmp;
end
clear tmp;


% % remove bubbles from the background
% images_size = size(images);
% imsize = images_size([1,2,4]);
% imsize2 = images_size;
% imsize2(3) = 1;
% for imind = 1:numimages
%     disp(['Segmenting tissue & removing background ', num2str(imind), '/', num2str(numimages)])
%     im = reshape(images(:, :, imind, :), imsize);
% 
%     % Convert image to grayscale.    
%     V = rgb2gray(im);    
% 
%     % Apply Laplacian to detect regions with structures.    
%     V = abs(imfilter(V,fspecial('laplacian')));    
% 
%     % Thresholding to get regions with variation.    
%     BW = imbinarize(V,graythresh(V));    
% 
%     % Closing for some smoothing.
%     BW = imclose(BW,strel('disk',3));    
% 
%     % Fill holes.    
%     BW = imfill(BW,'holes');    
% 
%     % Remove small objects.
%     BW = bwareaopen(BW,200);
%     
% %     figure()
% %     subplot 121
% %     imshow(im)
% %     subplot 122
% %     imshow(BW)
% %     pause;
%     
% %     imshow(final_mask)
% %     pause
%     final_mask = repmat(~BW, [1, 1, 3]);
%     im(final_mask) = 255;
% %     imshow(im)
% %     pause
%     images(:, :, imind, :) = reshape(im, imsize2);
% %     imshow(im);
% %     pause;
% end



% %Scale slice thickness.
% resamplingfactorZ = 2; 
% slicethickness_orig = slicethickness;
% slicethickness = slicethickness_orig/resamplingfactorZ;
% % Form original coordinate axes in physical units.
% [X,Y,Z] = meshgrid(pixelsize:pixelsize:pixelsize*imsize(2),pixelsize:pixelsize:pixelsize*imsize(1),slicethickness_orig:slicethickness_orig:slicethickness_orig*numimages);
% % Form new coordinate axes in physical units (i.e. query points).
% [Xq,Yq,Zq] = meshgrid(pixelsize:pixelsize:pixelsize*imsize(2),pixelsize:pixelsize:pixelsize*imsize(1),slicethickness_orig:slicethickness:slicethickness_orig*numimages);
% % Perform HSV transform for all images.
% hsvdata = zeros(size(images),'single');
% for imind = 1:size(images,3)    grid off; saveas(gcf, [outputdatapath,filesep,sampleID{i}, '_roi_and_tissue_color_bar.png']);
%     hsvdata(:,:,imind,:) = rgb2hsv(squeeze(images(:,:,imind,:)));
% end
% % Interpolate each HSV component separately to increase Z resolution.
% highres = zeros(imsize(1),imsize(2),numimages*resamplingfactorZ - 1,3,'single');
% for c = 1:3
%     highres(:,:,:,c) = interp3(X,Y,Z,hsvdata(:,:,:,c),Xq,Yq,Zq,'linear');
% end
% clear hsvdata;
% % Transform high-res HSV back to RGB.
% for imind = 1:size(highres,3)
%     highres(:,:,imind,:) = hsv2rgb(squeeze(highres(:,:,imind,:)));
% end


% % highlight slides 55 and 88 (011 prostate) on 
% marg = 100;
% images(marg:end-marg, marg:end-marg, 18, :) = 0;
% images(marg:end-marg, marg:end-marg, 29, :) = 0;


% figure('Renderer', 'painters', 'Position', [10 10 900 600])
set(gcf, 'Position', get(0, 'Screensize'));

% Render the volume.
h = vol3d('cdata',images,'texture','3D');
view(3);  
axis tight;
% flip Z axis. Do not flip to stay consistent with previous publications
% set(gca,'Zdir','reverse')
% flip x axis to have data as it was originally
set(gca,'Xdir','reverse')

% Adjust Z-scaling.
daspect([1 1 zscaling*pixelsize/slicethickness]);

% Adjust opacity.
alphamap('rampup');
alphamap(opacity*alphamap);
% alphamap('decrease');

% Set tick marks as millimeters.
xticks((1/pixelsize)*[0 2000 4000 6000 8000 10000 12000 14000]); xticklabels({'0','2','4','6','8','10','12', '14'}); xlabel('mm');
yticks((1/pixelsize)*[0 2000 4000 6000 8000 10000 12000 14000]); yticklabels({'0','2','4','6','8','10','12', '14'}); ylabel('mm');
zticks((1/slicethickness)*[0 500 1000 1500 2000 2500 3000 3500 4000 4500 5000]); zticklabels({'0','0.5','1','1.5', '2', '2.5', '3', '3.5', '4', '4.5', '5'}); zlabel('mm');
set(gca,'FontSize',17) 

