# @String source_dir
# @String target_dir
# @String transforms_dir
# @String reference_name
# @boolean interpolate
# @boolean use_shrinking_constraint
# @int features_model_index
# @int registration_model_index
# @double rod
# @double min_inlier_ratio
# @double max_epsilon
# @int fd_bins
# @int fd_size
# @double initial_sigma
# @int max_octave_size
# @int min_octave_size
# @int steps
# @int mode
# @int img_subsamp_fact
# @double consistency_weight
# @double curl_weight
# @double div_weight
# @double image_weight
# @double landmark_weight
# @int max_scale_deformation
# @int min_scale_deformation
# @double stop_threshold
#
from ij import IJ
from register_virtual_stack import Register_Virtual_Stack_MT

# Redirect error messages to ImageJ log to avoid pausing in case of error.
IJ.redirectErrorMessages()

# Set success flag to false.
runsuccess = False

# Set parameters via the parameter object.
p = Register_Virtual_Stack_MT.Param()
# Interpolate or not.
p.interpolate = interpolate
# Transformation type.
p.registrationModelIndex = registration_model_index

# Parameters for RANSAC model fitting.
# Transformation type.
p.featuresModelIndex = features_model_index
# Closest/next closest neighbor distance ratio.
p.rod = rod
# Inlier/candidates ratio.
p.minInlierRatio = min_inlier_ratio
# Maximal allowed alignment error in pixels.
p.maxEpsilon = max_epsilon

# Parameters for SIFT.
# Number of orientation bins.
p.sift.fdBins = fd_bins
# Feature descriptor size.
p.sift.fdSize = fd_size
# Initial sigma of each scale octave.
p.sift.initialSigma = initial_sigma
# The "maximum image size".
p.sift.maxOctaveSize = max_octave_size
# The "minimum image size".
p.sift.minOctaveSize = min_octave_size
# Steps per scale octave.
p.sift.steps = steps

# bUnwarpJ parameters.
# Mode.
p.elastic_param.mode = mode
# Subsampling factor.
p.elastic_param.img_subsamp_fact = img_subsamp_fact
# Consistency weight.
p.elastic_param.consistencyWeight = consistency_weight
# Curl weight.
p.elastic_param.curlWeight = curl_weight
# Divergence weight.
p.elastic_param.divWeight = div_weight
# Image weight.
p.elastic_param.imageWeight = image_weight
# Landmark weight.
p.elastic_param.landmarkWeight = landmark_weight
# Maximum scale deformation.
p.elastic_param.max_scale_deformation = max_scale_deformation
# Minimum scale deformation.
p.elastic_param.min_scale_deformation = min_scale_deformation
# Stopping threshold.
p.elastic_param.stopThreshold = stop_threshold

# Run the registration.
try:
  Register_Virtual_Stack_MT.exec(source_dir, target_dir, transforms_dir, reference_name, p, use_shrinking_constraint)
  runsuccess = True
except:
  pass

# Output success flag as a one pixel image for Matlab to read.
if runsuccess:
  impout = IJ.createImage("runsuccess","8-bit white",1,1,1)
else:
  impout = IJ.createImage("runsuccess","8-bit black",1,1,1)
impout.show()