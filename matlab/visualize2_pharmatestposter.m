% 3D features, full res input
inputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE';
outputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE_visualizations';
images_dir_name='full_res_jp2';
no_bubbles_dir_name='full_res_subsampled10_no_bubbles';
tumormasks_dir_name='tumormasks';
features_dir_name='matlab_features';
ext='.jp2';
extmask='.png';
fullrespixelsize=0.92; % inputs have been subsampled to 50% and then again to 10% of original dims
zscaling=0.5; % !!!
slicethickness=50;
smoothingfactor=5;
reductionlevel=5;


% % Pharmatest 1
% inputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/EMAP_pharmatest/';
% outputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/EMAP_pharmatest_visualizations';
% images_dir_name='full_res_subsampled6';
% tumormasks_dir_name='';
% features_dir_name='';
% ext='.tiff';
% extmask='.png';
% fullrespixelsize=0.92/0.10; % inputs have been subsampled to 50% and then again to 10% of original dims
% zscaling=0.5; % !!!
% slicethickness=50;
% smoothingfactor=5;
% reductionlevel=0;

% % Pharmatest poster
% inputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/EMAP_pharmatest';
% outputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/EMAP_pharmatest_visualizations';
% images_dir_name='full_res_subsampled6';
% tumormasks_dir_name='masks_smo2';
% features_dir_name='';
% no_bubbles_dir_name='';
% ext='.tiff';
% extmask='.png';
% fullrespixelsize=0.221089108910891 * 2^2 * (1 / 0.06); % rl2 & subsampled6
% zscaling=1.0; % !!!
% slicethickness=100;
% smoothingfactor=5;
% reductionlevel=0;


% % GP17
% % inputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/GP17';
% % outputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/GP17_visualizations';
% images_dir_name='preprocessed';
% tumormasks_dir_name='tumormasks';
% features_dir_name='features';
% ext='.tiff';
% extmask='.tiff';
% fullrespixelsize=0.92/0.10; % inputs have been subsampled to 50% and then again to 10% of original dims
% zscaling=0.5; % !!!
% slicethickness=50;
% smoothingfactor=5;
% reductionlevel=0;


% % 3D features, subsampled10 input
% inputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE';
% outputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE_visualizations';
% images_dir_name='full_res_subsampled10';
% no_bubbles_dir_name='full_res_subsampled10_no_bubbles';
% tumormasks_dir_name='tumormasks_subsampled10';
% features_dir_name='USE_FULL_RES_DATA';
% ext='.tiff';
% extmask='.png';
% fullrespixelsize=0.92/0.10; % inputs have been subsampled to 50% and then again to 10% of original dims
% zscaling=0.5; % !!!
% slicethickness=50;
% smoothingfactor=5;
% reductionlevel=0;


% % coordinates [x, y], in half size (0.92 microns/pixel)
% POIs = {                                     
%     {'anatomical center', 'Lat-186_PTENX036-20-37_18114_46028_7452.tiff.jp2', 7500.01171875, 7071.949218750002, 'PTENX036-20'}                                             
%     {'anatomical center', 'Lat-186_PTENX036-15-43_19599_20556_9932.tiff.jp2', 8178.8662109375, 10918.8955078125, 'PTENX036-15'}                                             
%     {'anatomical center', 'Lat-186_Ptenx036-14-37_3855_4284_4156.tiff.jp2', 7430.30859375, 5981.800781249998, 'PTENX036-14'}                                             
%     {'anatomical center', 'Lat-186_PtenX036-011-28_17678_21900_24556.tiff.jp2', 5602.433593749999, 7132.015625, 'PTENX036-011'}                                            
%     {'anatomical center', 'LAT-186_PTENX036-033-37_6716_3964.tiff.jp2', 6883.2841796875, 10773.7373046875, 'PTENX036-033'}                                             
%     {'anatomical center', 'LAT-186_PTENX036-032-37_28079_4076_5340.tiff.jp2', 10925.0361328125, 5144.6630859375, 'PTENX036-032'}                                              
% };    

% POIs = {
%     {'anatomical center', 'Transformed_Preprocessed_landmarks_Lat-186_PtenX036-011-28_17678_21900_24556.png', 4312.0, 4579.3, 'PTENX036-011' }
%     {'anatomical center', 'Transformed_Preprocessed_landmarks_Lat-186_Ptenx036-14-37_3855_4284_4156.png', 4784.9, 5760.6, 'PTENX036-14' }
%     {'anatomical center', 'Transformed_Preprocessed_landmarks_Lat-186_PTENX036-15-43_19599_20556_9932.png', 5957.7, 7154.0, 'PTENX036-15' }
%     {'anatomical center', 'Transformed_Preprocessed_landmarks_Lat-186_PTENX036-20-37_18114_46028_7452.png', 8914.3, 5882.2, 'PTENX036-20' }
%     {'anatomical center', 'Transformed_Preprocessed_landmarks_LAT-186_PTENX036-032-37_28079_4076_5340.png', 7007.9, 4933.0, 'PTENX036-032' }
%     {'anatomical center', 'Transformed_Preprocessed_landmarks_LAT-186_PTENX036-033-37_6716_3964.png', 5846.9, 4618.0, 'PTENX036-033' }
% };

POIs = {
};


% % 3D
% sampleID = {'PTENX036-011', ...
%               'PTENX036-14', ...
%               'PTENX036-20', ...
%               'PTENX036-32', ...
%               'PTENX036-33', ...
%               'PTENX036-15'};


% sampleID = {'PTENX036-011',
%             'PTENX036-14',
%             'PTENX036-15',
%             'PTENX036-20',
%             'PTENX036-21',
%             'PTENX036-32',
%             'PTENX036-33',
%             'PTENX036-34',
%             'PTENX036-39',
%             'PTENX036-40',
%             'PTENX036-42',
%             'PTENX036-68',
%             'UTU186-xhiMyc-003',
%             'UTU186-xhiMyc-006',
%             'UTU186-xhiMyc-007',
%             'UTU186-xhiMyc-008',
%             'UTU186-xhiMyc-022',
%             'UTU186-xhiMyc-045',
%             'UTU186-xhiMyc-199',
%             'UTU186-xhiMyc-211',
%             'UTU186-xhiMyc-223',
%             'UTU186-xhiMyc-355',
%             'UTU186-xhiMyc-356',
%             'UTU186-xhiMyc-1387'};


sampleID = {'PTENX036-14'};

% % Pharmatest
% sampleID = {'pieces1_rl2', ...
%             'pieces2_rl3', ...
%             'pieces3_rl2'};

% sampleID = {'pieces1_rl2'};


% GP17
% sampleID = {'34623'};
% sampleID = {'34624'};
% sampleID = {'34625'};
% sampleID = {'34623', '34624', '34625'};

for i = 1:length(sampleID)
    
    inputpath = [inputdatapath,filesep,sampleID{i},filesep,images_dir_name];
    inputpath_no_bubbles = [inputdatapath,filesep,sampleID{i},filesep,no_bubbles_dir_name];
    inputpathmask = [inputdatapath,filesep,sampleID{i},filesep,tumormasks_dir_name];
    inputpathfeatures = [inputdatapath,filesep,sampleID{i},filesep,features_dir_name];
    
    % Tissue *only
    opacity = 1;
    % inputpath,ext,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity
    visualize_tissue_3D(inputpath, ext, fullrespixelsize, slicethickness, zscaling, reductionlevel, opacity)
    alphamap('rampup');
    tempmap = alphamap;
    tempmap(1:5) = 0;
    alphamap(opacity*tempmap);
%     alphamap(sqrt(tempmap))
%     axis off
    legend off
    grid on
%     saveas(gcf, [sampleID{i}, '_tissueonly.png'])
%     close;
    load('pharmatest_poster.mat')
    set(gca, 'CameraPosition', pharmatest_poster_campos)
    set(gca, 'CameraUpVector', pharmatest_poster_camupvec)
    set(gca,'xdir','reverse')
    set(gca,'ydir','reverse')
    xlim(xli)
    ylim(yli)
    zlim(zli)
%     saveas(gcf, [sampleID{i}, '_tissueonly.png'])
%     close;

    
%         % Tissue *only no bubbles
%     opacity = 1;
%     % inputpath,ext,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity
%     visualize_tissue_3D(inputpath_no_bubbles, ext, fullrespixelsize, slicethickness, zscaling, reductionlevel, opacity)
%     alphamap('rampup');
% %     tempmap = alphamap;
% %     tempmap(1:5) = 0;
% %     alphamap(opacity*tempmap);
% %     alphamap(sqrt(tempmap))
%     axis off
% %     
% %     load([sampleID{i} '_camerapos_tissueonly.mat'])
% %     set(gca, 'CameraPosition', pos)
% %     camorbit(-70, 0)
% %     saveas(gcf, [sampleID{i}, '_tissueonly.png'])
% %     close;


    
% %     ROI objects + tissue
%     opacity = 0.5; 
%     visualize_roimasks_3D(inputpath,ext,inputpathmask,extmask,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'alltissue');
%     alphamap('rampup');
%     tempmap = alphamap;
%     tempmap(1:5) = 0;
%     alphamap(opacity*tempmap);
%     set(gcf, 'Position', get(0, 'Screensize'));
% %     axis off
%     legend off
% %     savefig(gcf,[outputdatapath,filesep,sampleID{i},'_alltissue.fig'],'compact');
% %     visualize_video_3D([outputdatapath,filesep,sampleID{i},'_alltissue.mp4']);
% %     close all;
%     load('pharmatest_poster.mat')
%     set(gca, 'CameraPosition', pharmatest_poster_campos)
%     set(gca, 'CameraUpVector', pharmatest_poster_camupvec)
%     set(gca,'xdir','reverse')
%     set(gca,'ydir','reverse')
%     saveas(gcf, [sampleID{i}, '_densetissue_and_roi.png'])
%     close;


% %     ROI objects + tissue edges + center of mass cross
%     opacity = 1.0;
% %     visualize_roimasks_3D(inputpath_no_bubbles,ext,inputpathmask,extmask,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'tissueedges', POIs);
%     visualize_roimasks_3D(inputpath,ext,inputpathmask,extmask,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'tissueedges', POIs);
%     alphamap('rampup');
%     tempmap = alphamap;
%     tempmap(1:5) = 0;
%     alphamap(opacity*tempmap);
%     set(gcf, 'Position', get(0, 'Screensize'));
% %     axis off
%     legend off
%     load('pharmatest_poster.mat')
%     set(gca, 'CameraPosition', pharmatest_poster_campos)
%     set(gca, 'CameraUpVector', pharmatest_poster_camupvec)
%     set(gca,'xdir','reverse')
%     set(gca,'ydir','reverse')
%     saveas(gcf, [sampleID{i}, '_tissueedges_and_roi.png'])
%     lightangle(0,70);
%     lightangle(90,70);
%     lightangle(180,70);
%     lightangle(270,70);
%     lighting gouraud;
%     saveas(gcf, [sampleID{i}, '_tissueedges_and_roi_with_lighting.png'])
%     close;
    
    
%     savefig(gcf,[outputdatapath,filesep,sampleID{i},'_alltissue.fig'],'compact');
%     visualize_video_3D([outputdatapath,filesep,sampleID{i},'_alltissue.mp4']);
%     close all;
% 
%     % Shoot pics
%     % side view
%     load PTENX036-011_camerapos_pos1.mat
%     set(gca, 'CameraPosition', pos1)
%     % flip mouse 32
%     if strcmp(sampleID, 'PTENX036-32')
%         camorbit(180, 0)
%     end
%     saveas(gcf, [sampleID{i}, '_pos1.png'])
%     
%     % top view
%     load PTENX036-011_camerapos_pos2.mat
%     load([sampleID{i}, '_CamUpVec'])
%     set(gca, 'CameraPosition', pos2)
%     set(gca, 'CameraUpVector', camupvec)
%     % flip mouse 32
%     if strcmp(sampleID, 'PTENX036-32')
%         camorbit(180, 0)
%     end
%     saveas(gcf, [sampleID{i}, '_pos2.png'])
%     set(gca, 'CameraUpVectorMode', 'auto')
%  
%     % other side view
%     load PTENX036-011_camerapos_pos3.mat
%     set(gca, 'CameraPosition', pos3)
%     saveas(gcf, [sampleID{i}, '_pos3.png'])
%     
%     close;
    
    
% %     ROI objects.
%     opacity = 0.5;
%     visualize_roimasks_3D(inputpath,ext,inputpathmask,extmask,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'roiobjects');
%     load('pharmatest_poster.mat')
%     set(gca, 'CameraPosition', pharmatest_poster_campos)
%     set(gca, 'CameraUpVector', pharmatest_poster_camupvec)
%     set(gca,'xdir','reverse')
%     set(gca,'ydir','reverse')
%     xlim(xli)
%     ylim(yli)
%     zlim(zli)
% %     axis off
%     legend off
%     saveas(gcf, [sampleID{i}, '_roiobjects.png'])
%     close;
    
%     title(sampleID{i});

%     % Automatic angles
%     set(gcf, 'Position', get(0, 'Screensize'));
%     load PTENX036-011_camerapos_pcaaxis_top.mat
%     set(gca, 'CameraPosition', pos)
%     saveas(gcf, [sampleID{i}, '_roiobject_pca_axes_top.png'])
%     load PTENX036-011_camerapos_pcaaxis_side.mat
%     set(gca, 'CameraPosition', pos)
%     saveas(gcf, [sampleID{i}, '_roiobject_pca_axes_side.png'])
%     load PTENX036-011_camerapos_pcaaxis_side2.mat
%     set(gca, 'CameraPosition', pos)
%     saveas(gcf, [sampleID{i}, '_roiobject_pca_axes_side2.png'])
%     close;
% 
%     savefig(gcf,[outputdatapath,filesep,sampleID{i},'_roiobjects.fig'],'compact');
%     visualize_video_3D([outputdatapath,filesep,sampleID{i},'_roiobjects.mp4']);
%     close all;




% %     ROI object pictures.
%     opacity = 0.5;
%     visualize_roimasks_3D(inputpath,ext,inputpathmask,extmask,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'roiobject_pictures');
%     title(sampleID{i});
%     set(gcf, 'Position', get(0, 'Screensize'));
% %     savefig(gcf,[outputdatapath,filesep,sampleID{i},'_roiobjects.fig'],'compact');
% %     visualize_video_3D([outputdatapath,filesep,sampleID{i},'_roiobjects.mp4']);
% %     close all;


% %     ROI tissue.
%     opacity = 0.5;
%     visualize_roimasks_3D(inputpath,ext,inputpathmask,extmask,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'roitissue');
%     set(gcf, 'Position', get(0, 'Screensize'));
%     savefig(gcf,[outputdatapath,filesep,sampleID{i},'_roitissue.fig'],'compact');
% %     visualize_video_3D([outputdatapath,filesep,sampleID{i},'_roitissue.mp4']);
% %     close all;
    

% featureslist = {'nucNumber'
%                 'DensityLBP6'
%                 'mROI-BlockLBP-9H'
%                 'mROI-BlockLBP-6E'
%                 'NhoodMaxDist'
%                 'mROI-BlockLBP-6H'
%                 'NhoodNucAngleSkewABS'
%                 'ROI-BlockSIFT-ScaleMeanH'
%                 'NhoodNucAngleKurtABS'
%                 'NhoodStdDist'
%                 'SIFT-ScaleStdH'
%                 'mROI-BlockLBP-4H'
%                 'mROI-BlockLBP-9E'
%                 'mROI-BlockSIFT-ScaleMeanH'
%                 'NhoodNucAngleVar'
%                 'SIFT-ScaleMeanH'
%                 'NhoodSkewnessH'
%                 'meanNucSizeH'
%                 'meanNucDistInNucNB'
%                 'DensityLBP7'
%                 'NhoodMeanDist'};


% featureslist = {'nucNumber'                        
%                'DensityLBP6_Nuc_mean'             
%                'DensityLBP6_Nuc_std'              
%                'NhoodNucAngleVar_Nuc_mean'        
%                'NhoodNucAngleVar_Nuc_std'         
%                'meanNucDistInNucNB_Nuc_mean'      
%                'meanNucDistInNucNB_Nuc_std'       
%                'DensityLBP7_Nuc_mean'             
%                'DensityLBP7_Nuc_std'};
% 
% 
%     for f = 1:length(featureslist)
%         
%         % Features.
%         opacity = 0.5;
%     %     fullrespixelsize=0.92;
%     %     reductionlevel=5;
%         featurename = featureslist{f};
% %         featurename = 'mROI-BlockLBP-6E';
% %         featurename = 'NhoodMaxDist';
%         roionly = false;
%         visualize_features_3D(inputpath,ext,inputpathmask,extmask,inputpathfeatures,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,featurename,roionly);
%         axis off;
%         set(gcf, 'Position', get(0, 'Screensize'));
%         savefig(gcf,[outputdatapath,filesep,sampleID{i},'_',featurename,'_featurevolume.fig'],'compact');
%         saveas(gcf,[outputdatapath,filesep,sampleID{i},'_',featurename,'_featurevolume.png'])
%         % visualize_video_3D([outputdatapath,filesep,sampleID{i},'_',featurename,'.mp4']);
%         close all;
%     
%     end


%     % Features, ROI only.
%     opacity = 0.5;
%     fullrespixelsize=0.92;
%     reductionlevel=5;
%     featurename = 'nucNumber';
%     roionly = true;
%     visualize_features_3D(inputpath,ext,inputpathmask,extmask,inputpathfeatures,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,featurename,roionly);
%     set(gcf, 'Position', get(0, 'Screensize'));
%     savefig(gcf,[outputdatapath,filesep,sampleID{i},'_',featurename,'_roi.fig'],'compact');
% %     visualize_video_3D([outputdatapath,filesep,sampleID{i},'_',featurename,'_roi.mp4']);
%     close all;


end
    
