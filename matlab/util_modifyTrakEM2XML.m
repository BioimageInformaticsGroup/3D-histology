function util_modifyTrakEM2XML(inputXMLfile,oldinputpath,oldfilenames,outputXMLfile,newinputpath,newfilenames)
% util_modifyTrakEM2XML - Change the filenames in the TrakEM2 project XML
%
%   util_modifyTrakEM2XML(inputXMLfile,oldinputpath,oldfilenames,outputXMLfile,newinputpath,newfilenames)
%   Replaces the filenames in the TrakEM2 project XML to allow reapplying
%   previously estimated transformations to another set of images.
%
%   'inputXMLfile' is the full path to the original XML file.
%
%   'oldinputpath' is the path to the original image files.
%
%   'oldfilenames' is a cell array of strings containing the filenames of
%   the original image files.
%
%   'outputXMLfile' is the full path to the new modified XML file.
%
%   'newinputpath' is the path to the new image files. The old path will be
%   replaced by this.
%
%   'newfilenames' is a cell array of strings containing the filenames of
%   the new image files. The old ones will be replaced by these.
%

% Make sure that the number of old and new filenames matches.
assert(length(oldfilenames) == length(newfilenames),'Error: the number of old and new filenames does not match!');

% If the paths contain Windows file separators, change them to Linux style
% (which TrakEM2 uses).
oldinputpath = strrep(oldinputpath,'\','/');
newinputpath = strrep(newinputpath,'\','/');

% Read original data.
fid = fopen(inputXMLfile,'r');
txtdata = fread(fid,'*char')';
fclose(fid);

% Modify all of the filenames in the XML.
for i = 1:length(oldfilenames)
    txtdata = strrep(txtdata,[oldinputpath,oldfilenames{i}],[newinputpath,newfilenames{i}]);
end

% Output modified XML file.
fid = fopen(outputXMLfile,'w');
fprintf(fid,'%s',txtdata);
fclose(fid);