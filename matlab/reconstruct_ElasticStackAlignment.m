function [runsuccess] = reconstruct_ElasticStackAlignment(inputpath_images,inputpath_masks,inputpath_fiducials,outputpath_images,outputpath_masks,outputpath_fiducials,transformpath,temppath,imagepixelsize,sectionthickness,ransac,sift,elastic)
% reconstruct_ElasticStackAlignment - Form 3D reconstruction using TrakEM2 plugin for ImageJ.
%
%   [runsuccess] = reconstruct_ElasticStackAlignment(inputpath_images,inputpath_masks,inputpath_fiducials,outputpath_images,outputpath_masks,outputpath_fiducials,transformpath,temppath,imagepixelsize,sectionthickness,ransac,sift,elastic)
%   Co-registers a stack of images using ElasticStackAlignment via TrakEM2 
%   for ImageJ. The ImageJ-MATLAB interfacing package and Fiji installation
%   are required for running the code. Returns 1 if the run was successful,
%   otherwise returns zero.
%   
%   General settings:
%       'inputpath_images' is the path to the actual input image files.
%
%       'inputpath_masks' is the path to binary mask files specifying the
%       region of interest in each actual image.
%
%       'inputpath_fiducials' is the path to the fiducial image files
%       containing fiducial markers for the images.
%
%       'outputpath_images' is the path for saving the actual registered image files.
%
%       'outputpath_masks' is the path for saving registered binary mask
%       images.
%
%       'outputpath_fiducials' is the path for saving the registered fiducial
%       image files.
%
%       'transformpath' is the path for saving the estimated transformations as
%       .xml files.
%
%       'temppath' is the path for saving temporary files.
%
%       'imagepixelsize' is the pixel size.
%
%       'sectionthickness' is the section thickness in same units as pixel
%       size.
%
%   ransac: a struct with the following fields:
%       'features_model_index' specifies which model to use for RANSAC.
%       0=translation, 1=rigid, 2=similarity, 3=affine.
%
%       'rod' is the maximum closest/next closest match distance ratio for
%       excluding ambiguous feature matches. Default 0.92.
%
%       'min_inlier_ratio' is the minimum ratio of inliers for an accepted
%       model. Models with lower inlier ratio are rejected. Default 0.05.
%
%       'max_epsilon' is the maximum error relative to the model allowed
%       for inliers (in pixels). Default 10 % of image size.
%
%       'min_num_inliers' is the minimum absolute number of inliers, eg. 12
%
%       'rejectidentity' is either 'true' or 'false', controlling whether
%       to reject identity transformations. Should only be set to true if 
%		there are some constant background patterns from image to image,
%		such as dirt on the microscope lens etc.
%
%       'identitytolerance' is the tolerance in pixels to use for rejecting
%       identity transformations.
%
%   sift: a struct with the following fields:
%       'fd_bins' is the number of orientation bins. Default 8.
%
%       'fd_size' is the size of the descriptor (pixels). Default 4.
%
%       'initial_sigma' is the initial Gaussian blur std (pixels). Default 1.6.
%
%       'max_octave_size' is the largest scale to use. Decrease this to
%       exclude smaller objects. Default 1024 pixels.
%
%       'min_octave_size' is the minimum scale to use. Increase this to
%       exclude larger shapes. Default 64 pixels.
%
%       'steps' is the number of steps per scale octave. Default 3.
%
%   elastic: a struct with the following fields:
%       'maxnumfailures' is the maximum number of sections to skip due to
%       bad alignment results before stopping completely matching to further
%		sections.
%		Example: 3.
%
%       'maxnumneighbors' is the maximum number of sections to consider
%       when aligning a section to its neighbors.
%		Example: 5.
%
%       'optimization_model_index' specifies which model to use for pre-alignment.
%       0=translation, 1=rigid, 2=similarity, 3=affine. Rigid models are recommended
%		since the global optimum of similarity and affine models is a zero-size image.
%		Example: 1.
%
%       'maxiterationsoptimize' is the maximum number of iterations for
%       optimization of pre-alignment.
%		Example: 1000.
%
%       'maxplateauwidthoptimize' is the maximum plateau width for
%       optimization of pre-alignment. The optimizer won't stop before it has
%		continued for the length of the plateau even if the error is not decreasing
%		anymore. This protects against getting stuck in small local optima.
%		Example: 200.	
%
%       'layerscale' specifies the scale factor used for matching the images.
%		In other words, the images can be upscaled or downscaled first.
%		The pixel size of the images should be approximately as large or larger
%		than section spacing. For images that have been already downsampled to a 
%		suitable resolution, set this to 1.
%		Example: 0.25.
%
%       'searchradius' specifies the maximum search area for block matching in pixels.
%		It should be large enough to capture the largest deformations but as small as
%		possible to increase speed and to prevent false matches.
%		Example: 12.
%
%       'blockradius' specifies the size of the blocks used in block matching in pixels.
%		It should be large enough to include recognizable structures but not too large.
%		Usually, setting blockradius == searchradius is a good choice.
%		Example: 600.
%
%       'resolutionspringmesh' specifies the number of vertices in a long row of the
%		spring mesh used for the final optimization.
%		Example: image width/8192 x 32, would be aroung 4 for prostate subsample16.
%
%       'minr' specifies the minimum PMCC correlation coefficient for
%		accepted matches between blocks. Candidate matches with lower 
%		correlation are not true matches since they are not similar enough.	
%		Higher values will lead to less false positives but fewer matches.
%		Example: 0.6
%
%       'maxcurvaturer' specifies the maximum curvature of the PMCC correlation
%		surface at a match. High curvature points represent edges, which cannot
%		be matched uniquely. The value must be > 1. Higher values will accept more
%		matches alongside elongated structures and thus lead to potentially more
%		false positives.
%		Example: 10.
%
%       'rodr' specifies the threshold for rejecting ambiguous block
%		matches. It it defined as the maximal second best r/best r. Higher values
%		will accept more potentially ambiguous matches that may be false positives.
%		Example: 0.92.
%
%       'localmodelindex' specifies the local linear transformation type used for 
%		rejecting outlier matches which do not fit well to a local model computed 
%		from neighbouring points. Usually a rigid model should be used.
%		0=translation, 1=rigid, 2=similarity, 3=affine.
%		Example: 3.
%
%       'localregionsigma' specifies the standard deviation of the Gaussian radial
%		distribution used for weighting the matches in a local neighborhood when
%		estimating a local transformation.
%		Example: 1000.
%
%       'maxlocalepsilon' specifies the threshold for rejecting matches that differ 
%		too much from a local transformation computed within a neighborhood of matches.
%		The match is rejected as an outlier if its offset from the model is more than
%		this value in pixels. In other words, this is the absolute threshold for outliers.
%		Example: 1.0.
%
%       'maxlocaltrust' specifies the multiplier threshold for rejecting matches that
%		differ too much from other matches used to compute a local transformation within
%		a neighborhood of matches. The parameter is defined as a multiplier relative to
%		the average error of all weighted matches relative to the local transformation.
%		In other words, this is the relative threshold for outliers.
%		Example: 4.0.
%
%       'maxstretchspringmesh' specifies the maximal stretch of springs in the spring mesh
%		before they will disrupt.
%		Example: 2000.
%
%       'dampspringmesh' specifies the damping constant of the spring mesh (?).
%		Example: 0.9.
%
%       'stiffnessspringmesh' specifies the spring constant of the spring mesh. Larger values
%		will lead to less deformed (more strictly regularized) solutions while small values may
%		result in unevenly distributed deformation due to premature relaxation of the simulation.
%		Example: 0.25.
%
%       'maxplateauwidthspringmesh' is the maximum plateau width for
%       optimization of the spring mesh. The optimizer won't stop before it has
%		continued for the length of the plateau even if the error is not decreasing
%		anymore. This protects against getting stuck in small local optima.
%		Example: 1000.
%
%       'maxiterationsspringmesh' is the maximum number of iterations for optimization of the
%		spring mesh.
%		Example: 5000.
%
%		'uselegacyoptimizer' controls whether to use an older optimization method.
%		According to Saalfeld, the legacy optimizer sometimes converges faster and
%		even better, sometimes the new optimizer is better.
%		Example: false.

% Check if fiducial images should be transformed.
if isempty(inputpath_fiducials) || isempty(outputpath_fiducials)
    outputfiducials = false;
else
    outputfiducials = true;
end
% Check if mask images should be transformed.
if isempty(inputpath_masks) || isempty(outputpath_masks)
    outputmasks = false;
else
    outputmasks = true;
end

% Start ImageJ.
if ~exist('MIJ','var')
    % MATLAB cannot find Java3D components from the ImageJ classpath unless
    % they are explicitly added like this.
    javaaddpath('/bmt-data/bii/3Dprostate/j3d-1_5_2-linux-amd64/lib/ext/j3dcore.jar');
    javaaddpath('/bmt-data/bii/3Dprostate/j3d-1_5_2-linux-amd64/lib/ext/j3dutils.jar');
    javaaddpath('/bmt-data/bii/3Dprostate/j3d-1_5_2-linux-amd64/lib/ext/vecmath.jar');
    addpath('/bmt-data/bii/3Dprostate/Fiji.app/scripts');
    ImageJ(true,false);
end

% Get the filenames of input images.
[filenames_images, filenames_masks, filenames_fiducials] = util_getSortedFilenames(inputpath_images,inputpath_masks,inputpath_fiducials,'.tif');
numimages = length(filenames_images);

% Constant parameters.
numcores = feature('numcores');
sift.clearcache = 'true'; % Always clear the cache to be sure.
ransac.widestsetonly = 'false'; % Always use all inliers, not only the widest set (seems that this parameter does not even do anything).
ransac.multiplehypotheses = 'true'; % Actually, this does not do anything for the elastic alignment, it only concerns regularized affine alignment.
elastic.visualize = 'false'; % Never visualize results.
elastic.isaligned = 'false'; % Never skip pre-alignment.
elastic.uselocalsmoothnessfilter = 'true'; % Never skip local smoothness filter.

% Add file separator to paths, ImageJ wants it.
[inputpath_images,outputpath_images,transformpath,temppath] = util_addFilesep(inputpath_images,outputpath_images,transformpath,temppath);
if outputmasks == true
    [inputpath_masks,outputpath_masks] = util_addFilesep(inputpath_masks,outputpath_masks);
end
if outputfiducials == true
    [inputpath_fiducials,outputpath_fiducials] = util_addFilesep(inputpath_fiducials,outputpath_fiducials);
end

% Collect all parameters into a string.
macroParameters = ['source_dir=',inputpath_images,' target_dir=',outputpath_images,' transforms_dir=',transformpath,' imagepixelsize=',num2str(imagepixelsize),' sectionthickness=',num2str(sectionthickness), ...
                   ' siftfdbins=',num2str(sift.fd_bins),' siftfdsize=',num2str(sift.fd_size),' siftinitialsigma=',num2str(sift.initial_sigma),' siftmaxoctavesize=',num2str(sift.max_octave_size),' siftminoctavesize=',num2str(sift.min_octave_size), ...
                   ' siftsteps=',num2str(sift.steps),' clearcache=',sift.clearcache,' maxnumthreadssift=',num2str(numcores),' rod=',num2str(ransac.rod),' desiredmodelindex=',num2str(elastic.optimization_model_index), ...
                   ' expectedmodelindex=',num2str(ransac.features_model_index),' identitytolerance=',num2str(ransac.identitytolerance),' isaligned=',elastic.isaligned,' maxepsilon=',num2str(ransac.max_epsilon), ...
                   ' maxiterationsoptimize=',num2str(elastic.maxiterationsoptimize),' maxnumfailures=',num2str(elastic.maxnumfailures),' maxnumneighbors=',num2str(elastic.maxnumneighbors),' maxnumthreads=',num2str(numcores), ...
                   ' maxplateauwidthoptimize=',num2str(elastic.maxplateauwidthoptimize),' mininlierratio=',num2str(ransac.min_inlier_ratio),' minnuminliers=',num2str(ransac.min_num_inliers),' multiplehypotheses=',ransac.multiplehypotheses, ...
                   ' widestsetonly=',ransac.widestsetonly,' rejectidentity=',ransac.rejectidentity,' visualize=',elastic.visualize,' blockradius=',num2str(elastic.blockradius),' dampspringmesh=',num2str(elastic.dampspringmesh), ...
                   ' layerscale=',num2str(elastic.layerscale),' localmodelindex=',num2str(elastic.localmodelindex),' localregionsigma=',num2str(elastic.localregionsigma),' maxcurvaturer=',num2str(elastic.maxcurvaturer), ...
                   ' maxiterationsspringmesh=',num2str(elastic.maxiterationsspringmesh),' maxlocalepsilon=',num2str(elastic.maxlocalepsilon),' maxlocaltrust=',num2str(elastic.maxlocaltrust),' maxplateauwidthspringmesh=',num2str(elastic.maxplateauwidthspringmesh), ...
                   ' uselegacyoptimizer=',elastic.uselegacyoptimizer,' maxstretchspringmesh=',num2str(elastic.maxstretchspringmesh),' minr=',num2str(elastic.minr),' resolutionspringmesh=',num2str(elastic.resolutionspringmesh), ...
                   ' rodr=',num2str(elastic.rodr),' searchradius=',num2str(elastic.searchradius),' stiffnessspringmesh=',num2str(elastic.stiffnessspringmesh),' uselocalsmoothnessfilter=',elastic.uselocalsmoothnessfilter];

% Run registration plugin.
try
    MIJ.run('ElasticStackAlignment align', macroParameters);
catch
    runsuccess = 0;
    return;
end
% This is a hack to get the Boolean success indicator as a one-pixel image
% from ImageJ. TrakEM2 does not necessarily raise an exception even if it
% fails, so this is needed to make sure the script made it to the end.
runsuccess = MIJ.getImage('runsuccess');
runsuccess = runsuccess > 0;
MIJ.run('Close All');
if runsuccess == 0
    return;
end

% Verify that no excessive 'tissue drifting' has taken place. This can
% cause HUGE output images. Checking the first image should be enough.
inputinfo = imfinfo([inputpath_images,filenames_images{1}]);
inputsize = [inputinfo.Width inputinfo.Height];
outputinfo = imfinfo([outputpath_images,filenames_images{1}]);
outputsize = [outputinfo.Width outputinfo.Height];
if outputsize(1) > 5*inputsize(1) || outputsize(2) > 5*inputsize(2)
	% Clear the output image folder and stop.
	warning(['Output image size ',num2str(outputsize(1)),'x',num2str(outputsize(2)),' exceeds limit ',num2str(5*inputsize(1)),'x',num2str(5*inputsize(2)),', aborting!']);
    delete([outputpath_images,'*.tif']);
    MIJ.run('Close All');
    runsuccess = 0;
    return;
end

% Transform masks.
if outputmasks == true
    util_modifyTrakEM2XML([transformpath,'tempproject.xml'],inputpath_images,filenames_images,[transformpath,'tempproject_masks.xml'],inputpath_masks,filenames_masks);
    MIJ.run('ElasticStackAlignment reapplytransform',['source_dir=',inputpath_masks,' target_dir=',outputpath_masks,' projectfile_path=',[transformpath,'tempproject_masks.xml'],' isimagecolor=false']);
    MIJ.run('Close All');
    delete([transformpath,'tempproject_masks.xml']);
end

% Transform fiducial images.
if outputfiducials == true
    numfiducialimages = length(filenames_fiducials);
    % There is one fiducial image per image.
    if numfiducialimages == numimages
        % Transform the fiducial images.
        util_modifyTrakEM2XML([transformpath,'tempproject.xml'],inputpath_images,filenames_images,[transformpath,'tempproject_fiducials.xml'],inputpath_fiducials,filenames_fiducials);
        MIJ.run('ElasticStackAlignment reapplytransform',['source_dir=',inputpath_fiducials,' target_dir=',outputpath_fiducials,' projectfile_path=',[transformpath,'tempproject_fiducials.xml'],' isimagecolor=true']);
        MIJ.run('Close All');
        delete([transformpath,'tempproject_fiducials.xml']);

    % There are two fiducial images per image.
    elseif numfiducialimages == 2*numimages-2
        % Copy the first half of the fiducial images into temp folder.
        for imind = [1 2:2:numfiducialimages]
            [success,message,~] = copyfile([inputpath_fiducials,filenames_fiducials{imind}],temppath);
            if success == 0
                error(['Copying fiducial image to temp folder failed: ',message]);
            end
        end

        % Transform the fiducial images.
        [~,~, filenames_fiducials_temp] = util_getSortedFilenames(inputpath_images,inputpath_masks,temppath,'.tif');
        util_modifyTrakEM2XML([transformpath,'tempproject.xml'],inputpath_images,filenames_images,[transformpath,'tempproject_fiducials1.xml'],temppath,filenames_fiducials_temp);
        MIJ.run('ElasticStackAlignment reapplytransform',['source_dir=',temppath,' target_dir=',outputpath_fiducials,' projectfile_path=',[transformpath,'tempproject_fiducials1.xml'],' isimagecolor=true']);
        MIJ.run('Close All');
        delete([transformpath,'tempproject_fiducials1.xml']);

        % Clear the temp folder.
        delete([temppath,'*.tif']);

        % Copy the second half of the fiducial images into temp folder.
        for imind = [1 3:2:numfiducialimages numfiducialimages]
            [success,message,~] = copyfile([inputpath_fiducials,filenames_fiducials{imind}],temppath);
            if success == 0
                error(['Copying fiducial image to temp folder failed: ',message]);
            end
        end

        % Transform the fiducial images.
        [~,~, filenames_fiducials_temp] = util_getSortedFilenames(inputpath_images,inputpath_masks,temppath,'.tif');
        util_modifyTrakEM2XML([transformpath,'tempproject.xml'],inputpath_images,filenames_images,[transformpath,'tempproject_fiducials2.xml'],temppath,filenames_fiducials_temp);
        MIJ.run('ElasticStackAlignment reapplytransform',['source_dir=',temppath,' target_dir=',outputpath_fiducials,' projectfile_path=',[transformpath,'tempproject_fiducials2.xml'],' isimagecolor=true']);
        MIJ.run('Close All');
        delete([transformpath,'tempproject_fiducials2.xml']);

        % Clear the temp folder.
        delete([temppath,'*.tif']);
    else
        error(['The number of images ',num2str(numimages),' and fiducial images ',num2str(numfiducialimages),' does not match!']);
    end
end

% Clean up the transforms folder.
if exist([transformpath,'tempproject.xml'],'file')
    delete([transformpath,'tempproject.xml']);
end
if length(dir(transformpath)) > 2
    rmdir([transformpath,'*'],'s');
end
