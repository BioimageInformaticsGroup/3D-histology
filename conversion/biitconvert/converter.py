import os
import sys
import logging
import argparse

import glymur
from PIL import Image
import numpy as np
# from scipy.misc import imsave
from skimage.transform import resize
from skimage.io import imsave

sys.path.insert(1, os.path.join(sys.path[0], '..'))  # find a better way to manage imports
sys.path.insert(0, "/home/memasva/Projects/Registration/3D-histology/")
sys.path.insert(0, "/home/memasva/Projects/Registration/3D-histology/openslide-python/")
print("TESTING SYS.PATH[0]: {}".format(sys.path[0]))

import biit_wsi_io.wsiio

Image.MAX_IMAGE_PIXELS = 5000000000

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


# Enum for interpolation method
class Interpolation:
    NEAREST_NEIGHBOR = 0
    BILINEAR = 1
    BICUBIC = 2


def parse_command_line_args():
    # parse arguments
    parser = argparse.ArgumentParser(description="Convert image formats")
    parser.add_argument('input', help="Input image")
    parser.add_argument('output', help="Output image")
    parser.add_argument('--resize', help="Resize image", default=None, type=float)
    parser.add_argument('--use-averaging', dest="averaging", help="Resize image with averaging", default=False, action="store_true")

    # define group 'interpolation' that allows only one of defined arguments to be used at a time
    interpolation = parser.add_mutually_exclusive_group(required=False)
    default_interpolation = Interpolation.BICUBIC
    interpolation.add_argument(
        '--bilinear',
        dest="interpolation",
        help="Select bilinear interpolation",
        default=default_interpolation,
        action='store_const',
        const=Interpolation.BILINEAR)
    interpolation.add_argument(
        '--bicubic',
        dest="interpolation",
        help="Select bicubic interpolation",
        default=default_interpolation,
        action='store_const',
        const=Interpolation.BICUBIC)
    interpolation.add_argument(
        '--nearest-neighbor',
        dest="interpolation",
        help="Select nearest-neighbor interpolation",
        default=default_interpolation,
        action='store_const',
        const=Interpolation.NEAREST_NEIGHBOR)

    args = vars(parser.parse_args())
    return args


def check_arguments(args):
    # if output file exists already, remove it first
    if os.path.exists(args["output"]):
        logger.warning("    Output path exists, removing file {}".format(args["output"]))
        os.remove(args["output"])

    output_dir = os.path.dirname(args["output"])
    if output_dir != "":
        os.makedirs(output_dir, exist_ok=True)


def resize_to_target_dims(im: np.ndarray, original_dimensions: tuple, target_size: tuple, interpolation: Interpolation):
    """ Resize image to target dimensions
    Args:
        target_size: (y, x)
    """
    # select interpolation method according to input form command line
    if interpolation == Interpolation.BILINEAR:
        interp_str = "bilinear"
        interp_order = 1
        pil_interp = Image.BILINEAR
    elif interpolation == Interpolation.BICUBIC:
        interp_str = "bicubic"
        interp_order = 3
        pil_interp = Image.BICUBIC
    elif interpolation == Interpolation.NEAREST_NEIGHBOR:
        interp_str = "nearest"
        interp_order = 0
        pil_interp = Image.NEAREST
    else:
        raise ValueError("Unrecognized interpolation method")

    logger.debug("\told dimensions X x Y: {}x{}".format(original_dimensions[1], original_dimensions[0]))
    logger.debug("\tnew dimensions X x Y: {}x{}".format(target_size[1], target_size[0]))

    # # resize with pil (takes alot of memory?)
    # pil_im = biit_wsi_io.wsiio.fromarray_large(im)
    # new_im = np.array(pil_im.resize((target_size[1], target_size[0]), resample=pil_interp))

    # scipy.misc.imresize (deprecated)
    # new_im = imresize(im, size=(target_size[1], target_size[0]), interp=interp_str)

    # skimage.transform.resize
    im = resize(im, (target_size[1], target_size[0]), order=interp_order)

    return im


def determine_rl_to_use(args):
    """Determine reductionlevel(rl) to use given the amount of total resizing (resize parameter)"""
    ext = os.path.splitext(args["input"])[-1]
    # determine used reduction level
    if args["resize"] is not None:
        # formats that support reading at reduced reolution, use reductionlevel parameter
        # otherwise read at full resolution and downscale
        if ext.lower() == ".ndpi" or ext.lower() == ".mrxs":
            # determine reduction level for reading at closest reolution that is higher than desired
            rl_optimal = np.log(100 / args["resize"]) / np.log(2)
            rl = int(np.floor(rl_optimal))
        else:
            rl_optimal = 1
            rl = 0
    else:
        rl = 0
        rl_optimal = None

    return rl, rl_optimal


def main():
    logger.debug("Starting biitconvert")

    args = parse_command_line_args()
    check_arguments(args)

    # print command line aguments
    logger.debug("Input arguments:")
    for k in args.keys():
        logger.debug("\t{}: {}".format(k, args[k]))

    logger.debug("Reading image..")
    rl, rl_optimal = determine_rl_to_use(args)

    # read image
    im, full_res_dimensions = biit_wsi_io.wsiio.biitwsi_imread(args["input"], reduction_level=rl)

    # resize to final size if needed
    if args["resize"] is not None and np.abs(rl - rl_optimal) > 0:
        target_resolution = np.round(np.array(full_res_dimensions)*(args["resize"] / 100)).astype(int)
        logger.debug(f"Resizing image (type: {type(im)})from dimensions {full_res_dimensions} to {target_resolution} ..")
        im = resize_to_target_dims(im, full_res_dimensions, target_resolution, args["interpolation"])
    
    # save to disk
    logger.info(f"Exporting image {args['input']} with resolution {im.shape} ..")
    biit_wsi_io.wsiio.biitwsi_imwrite(im, args["output"], compress_level=5)
    logger.info("Export successful")


if __name__ == '__main__':
    main()
