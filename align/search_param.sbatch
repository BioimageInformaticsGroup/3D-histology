#!/usr/bin/bash
#
# At first let's give job some descriptive name to distinct from other jobs
#SBATCH -J align
#
# Let’s redirect job’s out some other file than default slurm-%jobid-out
#SBATCH --output=logs/align_%j.out
#SBATCH --error=logs/align_%j.out
#
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=6
#
# time format  hours:minutes:seconds or days-hours
#SBATCH --time=6-23
#SBATCH --mem=60000
#SBATCH --partition=normal

fijipath=/bmt-data/bii/tools/Fiji.app

# Variables that are passed to this script as environment variables
# are denoted with upper case letters
# these are:
#    TARGETDIR
#    SOURCEDIR
#    TRANSFORMSDIR
#    LOOPCOUNTER
#    LAYERSCALEDIV
#    SIFTFDSIZE
#    SECTIONTHICKNESS
#    SIFTMINOCTAVESIZE
#    SIFTMAXOCTAVESIZE
#    MAXNUMNEIGHBORS
#    MAXEPSILON
#    ROD

# create target directory
echo "Creating ESA output directory: $TARGETDIR/full_res/$LOOPCOUNTER"
mkdir -p $TARGETDIR/full_res/$LOOPCOUNTER
# create transforms directory
echo "Creating transforms directory: $TRANSFORMSDIR/$LOOPCOUNTER"
mkdir -p $TRANSFORMSDIR/$LOOPCOUNTER

alignparams="\
$SOURCEDIR \
$TARGETDIR/full_res/$LOOPCOUNTER \
$TRANSFORMSDIR/$LOOPCOUNTER \
--layerscale $(bc -l <<< 1/$LAYERSCALEDIV) \
--maxepsilon $MAXEPSILON \
--siftmaxoctavesize $SIFTMAXOCTAVESIZE \
--siftminoctavesize $SIFTMINOCTAVESIZE \
--siftfdsize $SIFTFDSIZE \
--sectionthickness $SECTIONTHICKNESS \
--maxnumneighbors $MAXNUMNEIGHBORS \
--rod $ROD \
--minr $MINR \
--maxiterationsspringmesh $MAXITERATIONSSPRINGMESH \
--maxnumthreads 16 \
--maxnumthreadssift 16"

# Another prossibility to set these parameters:
#--siftmaxoctavesize $(bc <<< 20000/$LAYERSCALEDIV) \
#--maxepsilon $(bc -l <<< 35.090487*$LAYERSCALEDIV) \

# register images
$fijipath/ImageJ-linux64 --headless -- align.py $alignparams

echo "Registration for loop $LOOPCOUNTER done, begin subsampling"

# ensure target dir exists
echo "Creating subsampled directory $TARGETDIR/subsampled/$LOOPCOUNTER"
mkdir -p $TARGETDIR/subsampled/$LOOPCOUNTER

# write current params into the subsampled directory as a file for convenience
params_text_file=$TARGETDIR/registration_parameters_$LOOPCOUNTER.txt
echo "$alignparams" | sed "s/ /\n/g"> $params_text_file

echo "Finding from directory: $TARGETDIR/full_res/$LOOPCOUNTER"
find $TARGETDIR/full_res/$LOOPCOUNTER -maxdepth 1 -regextype sed -regex ".*\.tif" -printf "%f\n" | while read line; do
    echo "    Subsampling file: '$line'"
    # get filename without extension
    noext=$(echo "$line" | sed -E "s/\..*$//")

    # FIJICONVERT
    # positional arguments:
    #     input
    #     output
    # flags
    #     --resize <resize_amount>
    #     --use-averaging 
    #     --bilinear
    #     --bicubic
    #     --nearest-neighbor

    # echo "    fijiconvert input: $TARGETDIR/full_res/$LOOPCOUNTER/$line "
    # echo "    fijiconvert output: $TARGETDIR/subsampled/$LOOPCOUNTER/$noext.tif"

    $fijipath/ImageJ-linux64 --headless -- ../conversion/fijiconvert.py $TARGETDIR/full_res/$LOOPCOUNTER/$line $TARGETDIR/subsampled/$LOOPCOUNTER/$noext.tif --resize 10 --use-averaging

done

echo "Removing full_res output directory.."
rm -rf $TARGETDIR/full_res/$LOOPCOUNTER
echo "Removing transforms cache directory.."
rm -rf $TRANSFORMSDIR/$LOOPCOUNTER/trakem2*


