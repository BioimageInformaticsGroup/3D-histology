import logging
from pprint import pprint
from argparse import ArgumentParser

from PIL import Image

Image.MAX_IMAGE_PIXELS = 5000000000

import skimage
import numpy as np


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("path_landmark_image", help="path to coordinate file", type=str)
    parser.add_argument("path_marker_colors_npy", help="path to marker colors .npy file", type=str)
    # parser.add_argument("--radius", help="size of the disk", default=10, type=int)
    args = vars(parser.parse_args())
    return args


def read_landmarks_from_image(path, marker_colors):
    """ Load landmark image into a list of points [(y, x), (y, x), ...]

    threhsold transformed landmark image and take object centroids as transformed landmarks
    Args:
        path:  path to landmark image

    Returns:
        points (list(list(float, float)))
    """
    lnds_im = skimage.io.imread(path, as_gray=False, plugin="imageio")

    coords = []
    for m in marker_colors:
        idx = np.where(np.all(lnds_im == m, axis=-1))
        idx = np.array(idx).T
        m_coord = idx.mean(axis=0)
        coords.append(m_coord)

    # ensure that every point is unique
    assert len(coords) == len(marker_colors), "{} != {}".format(len(coords), len(marker_colors))
    return coords


def load_landmark_image(path, marker_colors):
    logging.info("Loading landmark image")
    try:
        points = read_landmarks_from_image(path, marker_colors)
    except AssertionError as e:
        logging.warning("Landmark image loading had errors")
        logging.warning(e)

    return [[p[1], p[0]] for p in points]


if __name__ == "__main__":
    logging.info("Loading landmark file..")
    args = parse_arguments()

    marker_colors = np.load(args["path_marker_colors_npy"])

    points = load_landmark_image(args["path_landmark_image"], marker_colors)
    pprint(points)

    logging.info("Landmark file loaded succesfully")
